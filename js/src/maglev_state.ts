import { Log } from './utils/log';
import {
  nodeIdFromPublicKeyFingerprint,
  FingerprintIdentityPeerAuth,
} from './auth/fingerprint_identity';
import { AuthContext } from './auth_context';
import { generateKeyPair, KeyPair } from './cryptov1/signing';
import { MaglevConfig } from './maglev_config';
import { ServiceHandlerDispatch } from './rpc/service_handler_dispatch';
import { Lazy } from './utils/lazy';
import { PeerState } from './peer_state';
import { Router } from './router';
import { lazyObservable, mapObservable } from './utils/observable';
import { Transport } from './transport/transport';
import { WSTransport } from './transport/ws_transport';
import { WebRtcTransport } from './transport/webrtc_transport';
import { RelayTransport } from './transport/relay_transport';
import { LoopbackTransport } from './transport/loopback_transport';
import { TrustedPeerAuth } from './auth/peer_auth';

const RELAY_NODE_ID = '<relay>';

export class MaglevState {
  constructor(public readonly config: MaglevConfig) {}

  public readonly log = new Log('maglev');

  public readonly authContext = new Lazy(async () => {
    const keyPair = await generateKeyPair();
    const nodeId = await nodeIdFromPublicKeyFingerprint(keyPair.publicKey);
    return new AuthContext(nodeId, keyPair);
  });

  public readonly nodeId = new Lazy(
    async () => (await this.authContext.get()).nodeId
  );

  // Reachability

  // Currently we use "healthy connection to relay" as a proxy for "reachable".
  public readonly reachable = lazyObservable(
    () => false,
    async () => {
      const transport = await this.relayPeerTransport.get();
      return transport.connected;
    }
  );

  // Router

  public readonly router = new Lazy(
    async () => new Router(await this.nodeId.get(), this)
  );

  // Relay

  private relayPeerTransport = new Lazy(async () => {
    const relayPeerState = new PeerState(
      RELAY_NODE_ID,
      this,
      new TrustedPeerAuth(),
      async (peerState) => [
        new WSTransport(
          peerState.nodeId,
          await this.authContext.get(),
          peerState.sessionContext,
          this.config.relay.url + '/v1/relay'
        ),
      ]
    );
    return (await relayPeerState.transports.get())[0];
  });

  // PeerState

  private peerStates = new Map<string, PeerState>();

  public getPeerState(nodeId: string): PeerState {
    let peerState = this.peerStates.get(nodeId);
    if (!peerState) {
      peerState = new PeerState(
        nodeId,
        this,
        new FingerprintIdentityPeerAuth(nodeId),
        (peerState) => this.initPeerTransports(peerState)
      );
      this.peerStates.set(nodeId, peerState);
    }
    return peerState;
  }

  private async initPeerTransports(peerState: PeerState): Promise<Transport[]> {
    const authContext = await this.authContext.get();

    // Self does loopback
    if (authContext.nodeId === peerState.nodeId) {
      return [
        new LoopbackTransport(
          peerState.nodeId,
          authContext,
          peerState.sessionContext
        ),
      ];
    }

    return [
      new WebRtcTransport(
        peerState.nodeId,
        authContext,
        peerState.sessionContext,
        this.config.wrtc!,
        peerState.nodeRef
      ),
      new RelayTransport(
        peerState.nodeId,
        authContext,
        peerState.sessionContext,
        await this.relayPeerTransport.get()
      ),
    ];
  }

  // ServiceHandlerDispatch

  public readonly serviceHandlerDispatch = new ServiceHandlerDispatch();

  private peerServiceHandlerDispatches = new Map<
    string,
    ServiceHandlerDispatch
  >();

  public getPeerServiceHandlerDispatch(nodeId: string): ServiceHandlerDispatch {
    let dispatch = this.peerServiceHandlerDispatches.get(nodeId);
    if (!dispatch) {
      dispatch = new ServiceHandlerDispatch(this.serviceHandlerDispatch);
      this.peerServiceHandlerDispatches.set(nodeId, dispatch);
    }
    return dispatch;
  }
}
