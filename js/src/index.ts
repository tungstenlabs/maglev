import { biChannel, channel } from './channels/buffered_channel';
import {
  Channel,
  ChannelReceiver,
  ChannelSender,
} from './channels/channel_interfaces';
import './gen/proto_gen';
import { Maglev } from './maglev';
import { NodeRef } from './node_ref';
import * as ProtoGenAPI from './proto_gen_api';
import { CallContext } from './proto_gen_api';
import { injectProtoGenApi } from './proto_gen_api_injected';
import { wlabs } from './gen/proto_gen';
import { ConnectionHealth } from './peer_state';
import { Log } from './utils/log';
import { Code, StatusError } from './transport/errors';

// Wire up injected generated proto code.
injectProtoGenApi(ProtoGenAPI.injected);

export {
  CallContext,
  Code,
  ConnectionHealth,
  Channel,
  ChannelReceiver,
  ChannelSender,
  Log,
  Maglev,
  NodeRef,
  ProtoGenAPI,
  StatusError,
  biChannel,
  channel,
  wlabs,
};
