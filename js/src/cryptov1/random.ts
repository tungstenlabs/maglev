import Long from 'long';
import { crypto } from './webcrypto';

export function getRandomUint8Array(len: number): Uint8Array {
  const array = new Uint8Array(len);
  crypto.getRandomValues(array);
  return array;
}

export function getRandomUint64(): Long {
  const bytes = getRandomUint8Array(8);
  return Long.fromBytes(Array.from(bytes));
}
