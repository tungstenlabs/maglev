import crypto from 'isomorphic-webcrypto';

export { crypto };

export type Buffer =
  | Int8Array
  | Int16Array
  | Int32Array
  | Uint8Array
  | Uint16Array
  | Uint32Array
  | Uint8ClampedArray
  | Float32Array
  | Float64Array
  | ArrayBuffer;
