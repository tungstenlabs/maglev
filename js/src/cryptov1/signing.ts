import { stringToBytes } from '../utils/string';
import { crypto, Buffer } from './webcrypto';

const ALGO_NAME = 'ECDSA';
const CURVE_NAME = 'P-256';
const HASH_NAME = 'SHA-256';
const KEY_ALGO: EcKeyAlgorithm = { name: ALGO_NAME, namedCurve: CURVE_NAME };
const SIG_PARAMS: EcdsaParams = { name: ALGO_NAME, hash: HASH_NAME };

export const SIGNATURE_LENGTH = 64; // 2 * 256-bit elements

type SignatureParts = (Buffer | string)[];

export interface PrivateKey {
  signParts(prefix: string, ...parts: SignatureParts): Promise<Uint8Array>;
}

export interface PublicKey {
  verifyParts(
    signature: Buffer,
    prefix: string,
    ...parts: Buffer[]
  ): Promise<boolean>;
  exportRaw(): Promise<Uint8Array>;
  equals(other: PublicKey): Promise<boolean>;
}

export type KeyPair = { privateKey: PrivateKey; publicKey: PublicKey };

export async function generateKeyPair(): Promise<KeyPair> {
  const pair = (await crypto.subtle.generateKey(KEY_ALGO, false, [
    'sign',
    'verify',
  ])) as CryptoKeyPair;
  return {
    privateKey: new WebCryptoPrivateKey(pair.privateKey),
    publicKey: new WebCryptoPublicKey(pair.publicKey),
  };
}

export async function importRawPublicKey(raw: Buffer): Promise<PublicKey> {
  return WebCryptoPublicKey.importRaw(raw);
}

export class WebCryptoPrivateKey implements PrivateKey {
  constructor(private readonly key: CryptoKey) {
    if (key.type != 'private') {
      throw new TypeError(`bad key type "${key.type}"`);
    }
  }

  public async signParts(
    prefix: string,
    ...parts: Buffer[]
  ): Promise<Uint8Array> {
    const payload = buildPayload(prefix, parts);
    return new Uint8Array(
      await crypto.subtle.sign(SIG_PARAMS, this.key, payload)
    );
  }

  public static async importRaw(raw: Buffer) {
    const key = await crypto.subtle.importKey('raw', raw, KEY_ALGO, false, [
      'sign',
    ]);
    return new WebCryptoPrivateKey(key);
  }
}

export class WebCryptoPublicKey implements PublicKey {
  constructor(public readonly key: CryptoKey) {
    if (key.type != 'public') {
      throw new TypeError(`bad key type "${key.type}"`);
    }
  }

  public verifyParts(
    signature: Buffer,
    prefix: string,
    ...parts: SignatureParts
  ): Promise<boolean> {
    const payload = buildPayload(prefix, parts);
    return crypto.subtle.verify(SIG_PARAMS, this.key, signature, payload);
  }

  public async exportRaw(): Promise<Uint8Array> {
    return new Uint8Array(await crypto.subtle.exportKey('raw', this.key));
  }

  public async equals(other: PublicKey): Promise<boolean> {
    const raw = await this.exportRaw();
    const otherRaw = await other.exportRaw();
    if (raw.length != otherRaw.length) {
      return false;
    }
    for (let i = 0; i < raw.length; i++) {
      if (raw[i] !== otherRaw[i]) {
        return false;
      }
    }
    return true;
  }

  public static async importRaw(raw: Buffer) {
    const key = await crypto.subtle.importKey('raw', raw, KEY_ALGO, true, [
      'verify',
    ]);
    return new WebCryptoPublicKey(key);
  }
}

function buildPayload(prefix: string, parts: SignatureParts): Uint8Array {
  const partArrays = [stringToBytes(prefix)];
  for (const part of parts) {
    if (typeof part === 'string') {
      partArrays.push(stringToBytes(part));
    } else {
      partArrays.push(new Uint8Array(part));
    }
  }

  const outLen = partArrays.reduce((tot, arr) => tot + 2 + arr.byteLength, 0);
  const out = new Uint8Array(outLen);

  let offset = 0;
  for (const partArray of partArrays) {
    // Encode len as big-endian uint16
    const len = partArray.byteLength;
    if (len > 0xffff) {
      throw new Error(`signature part length ${len} > max ${0xffff}`);
    }
    out[offset++] = len >>> 8;
    out[offset++] = len & 0xff;

    out.set(partArray, offset);
    offset += partArray.byteLength;
  }
  return out;
}
