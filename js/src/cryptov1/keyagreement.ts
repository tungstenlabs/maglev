import { Buffer, crypto } from './webcrypto';

const ALGO_NAME = 'ECDH';
const CURVE_NAME = 'P-256';
const KEY_ALGO: EcKeyAlgorithm = { name: ALGO_NAME, namedCurve: CURVE_NAME };

export interface PrivateKey<Pub extends PublicKey> {
  deriveSharedBytes(publicKey: Pub, length: number): Promise<Uint8Array>;
}

export interface PublicKey {
  exportRaw(): Promise<Uint8Array>;
}

export type KeyPair<Pub extends PublicKey> = {
  privateKey: PrivateKey<Pub>;
  publicKey: Pub;
};

export async function generateKeyPair(): Promise<WebCryptoKeyPair> {
  const pair = await crypto.subtle.generateKey(KEY_ALGO, false, ['deriveBits']);
  return {
    privateKey: new WebCryptoPrivateKey(pair.privateKey),
    publicKey: new WebCryptoPublicKey(pair.publicKey),
  };
}

export async function importRawPublicKey(
  raw: Buffer
): Promise<WebCryptoPublicKey> {
  const key = await crypto.subtle.importKey('raw', raw, KEY_ALGO, true, []);
  return new WebCryptoPublicKey(key);
}

export class WebCryptoPrivateKey implements PrivateKey<WebCryptoPublicKey> {
  constructor(private readonly key: CryptoKey) {
    if (key.type != 'private') {
      throw new TypeError(`bad key type "${key.type}"`);
    }
  }

  public async deriveSharedBytes(
    publicKey: WebCryptoPublicKey,
    length = 32
  ): Promise<Uint8Array> {
    return new Uint8Array(
      await crypto.subtle.deriveBits(
        { name: ALGO_NAME, public: publicKey.key },
        this.key,
        length * 8
      )
    );
  }
}

export class WebCryptoPublicKey implements PublicKey {
  constructor(public readonly key: CryptoKey) {
    if (key.type != 'public') {
      throw new TypeError(`bad key type "${key.type}"`);
    }
  }

  public async exportRaw(): Promise<Uint8Array> {
    return new Uint8Array(await crypto.subtle.exportKey('raw', this.key));
  }
}

export type WebCryptoKeyPair = KeyPair<WebCryptoPublicKey>;
