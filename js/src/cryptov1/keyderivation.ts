import { stringToBytes } from '../utils/string';
import { Buffer, crypto } from './webcrypto';

const ALGO_NAME = 'HKDF';
const HASH_NAME = 'SHA-256';

export async function deriveBytes(
  info: string,
  secret: Buffer,
  length: number
): Promise<Uint8Array> {
  const deriveKey = await crypto.subtle.importKey(
    'raw',
    secret,
    ALGO_NAME,
    false,
    ['deriveBits']
  );
  return new Uint8Array(
    await crypto.subtle.deriveBits(
      {
        name: ALGO_NAME,
        hash: HASH_NAME,
        info: stringToBytes(info),
        salt: new Uint8Array(),
      },
      deriveKey,
      length * 8
    )
  );
}
