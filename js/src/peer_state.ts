import { SessionContext } from './session_context';
import { Transport } from './transport/transport';
import { AsyncValue } from './utils/async_value';
import { NodeRef } from './node_ref';
import { MaglevState } from './maglev_state';
import { Lazy } from './utils/lazy';
import {
  lazyObservable,
  mapObservable,
  reduceObservables,
} from './utils/observable';
import { IncomingExchange, Session } from './session';
import { CallContext } from '.';
import { some } from './utils/optional';
import { PeerAuth } from './auth/peer_auth';

export class PeerState {
  public readonly nodeRef: NodeRef;

  constructor(
    public readonly nodeId: string,
    private readonly maglevState: MaglevState,
    private readonly peerAuth: PeerAuth,
    private readonly transportsInit: (
      peerState: PeerState
    ) => Promise<Transport[]>
  ) {
    this.nodeRef = new NodeRef(nodeId, maglevState);
  }

  // TODO: refactor to remove this indirection
  public readonly sessionContext: SessionContext = {
    sessionId: new AsyncValue(),
  };

  public readonly transports = new Lazy<Transport[]>(async () => {
    const transports = await this.transportsInit(this);
    const router = await this.maglevState.router.get();
    router.registerPeerTransports(this.nodeId, transports);
    return transports;
  });

  /// The highest-priority connected Transport (or null if none are connected)
  public readonly activeTransport = lazyObservable(
    () => null,
    async () => {
      const transports = await this.transports.get();
      return reduceObservables<boolean[], Transport | null>(
        transports.map((t) => t.connected),
        (connecteds) => {
          const activeIdx = connecteds.indexOf(true);
          return transports[activeIdx] ?? null;
        }
      );
    }
  );

  public readonly connectionHealth = lazyObservable<ConnectionHealth>(
    () => ConnectionHealth.UNHEALTHY,
    async () =>
      mapObservable(this.activeTransport, (activeTransport) =>
        activeTransport ? ConnectionHealth.HEALTHY : ConnectionHealth.UNHEALTHY
      )
  );

  public readonly session = new Lazy(async () => {
    const router = await this.maglevState.router.get();
    const session = new Session(
      this.nodeId,
      await this.maglevState.nodeId.get(),
      this.peerAuth,
      router.outbound,
      (incoming) => this.handleIncomingExchange(incoming)
    );
    session.newSessionEvent.addHandler(() => {
      this.sessionContext.sessionId.set(some(session.sessionId!));
    });
    this.activeTransport.observe((activeTransport) => {
      const results = activeTransport?.handshakeResults.get();
      if (results) {
        void session.handleHandshakeResults(results);
      }
    });
    return session;
  });

  private async handleIncomingExchange(incomingExchange: IncomingExchange) {
    const callContext: CallContext = {
      peerAuthInfo: incomingExchange.peerAuthInfo,
    };
    try {
      await this.maglevState
        .getPeerServiceHandlerDispatch(this.nodeId)
        .dispatch(incomingExchange.accept(), callContext);
    } catch (e) {
      this.maglevState.log.error('Error dispatching method call:', e);
    }
  }
}

export enum ConnectionHealth {
  /** There is a healthy transport. */
  HEALTHY,

  /** All transports are unhealthy. */
  UNHEALTHY,
}
