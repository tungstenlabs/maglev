import { MaglevState } from './maglev_state';
import { NodeRef } from './node_ref';

export class NodeStore {
  constructor(private maglevState: MaglevState) {}

  public async self(): Promise<NodeRef> {
    return this.get(await this.maglevState.nodeId.get());
  }

  public get(nodeId: string): NodeRef {
    return new NodeRef(nodeId, this.maglevState);
  }
}
