import { MaglevConfig, defaultMaglevConfig } from './maglev_config';
import { MaglevState } from './maglev_state';
import { NodeStore } from './node_store';

export class Maglev {
  /// The effective MaglevConfig with defaults resolved.
  public readonly config: MaglevConfig;

  /// The NodeStore, which is used to access NodeRefs.
  public readonly nodes: NodeStore;

  private readonly state;

  constructor(config: MaglevConfig) {
    this.config = { ...defaultMaglevConfig, ...config };
    this.state = new MaglevState(this.config);
    this.nodes = new NodeStore(this.state);
  }

  public get reachable() {
    return this.state.reachable;
  }

  /// @internal RPC
  public get serviceHandlerDispatch() {
    // HACK: this triggers relay connection startup
    void this.state.reachable.get();
    return this.state.serviceHandlerDispatch;
  }
}
