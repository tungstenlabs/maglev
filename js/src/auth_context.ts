import { KeyPair } from './cryptov1/signing';

/**
 * Represents and manages the authentication context for a Maglev instance. This
 * is a separate class only for organization, it is tightly coupled to `Maglev`.
 */
export class AuthContext {
  constructor(
    public nodeId: string,
    public keyPair: KeyPair,
    public credential?: Uint8Array
  ) {}
}
