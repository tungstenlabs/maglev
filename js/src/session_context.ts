import { AsyncValue } from './utils/async_value';

export interface SessionContext {
  sessionId: AsyncValue<Long>;
}
