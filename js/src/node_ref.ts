import { MaglevState } from './maglev_state';
import { ConnectionHealth, PeerState } from './peer_state';
export class NodeRef {
  constructor(
    public readonly nodeId: string,
    private readonly maglevState: MaglevState
  ) {}

  /// Attempt to connect to the node. If a timeout is given and the node cannot
  /// be reached before the timeout, connect resolves with false.
  public async connect(timeoutMs?: number): Promise<boolean> {
    return this.peerState.connectionHealth.whenEqual(
      ConnectionHealth.HEALTHY,
      timeoutMs
    );
  }

  /// @internal RPC
  public get serviceHandlerDispatch() {
    // HACK: this triggers relay connection startup
    void this.maglevState.reachable.get();
    return this.maglevState.getPeerServiceHandlerDispatch(this.nodeId);
  }

  /// @internal RPC
  public get session() {
    return this.peerState.session;
  }

  private get peerState(): PeerState {
    return this.maglevState.getPeerState(this.nodeId);
  }
}
