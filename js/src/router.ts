import { ChannelReceiver, ChannelSender } from './channels/channel_interfaces';
import { channel } from './channels/buffered_channel';
import { forward } from './channels/forward';
import {
  isTransportControlMsg,
  Transport,
  TransportMessage,
} from './transport/transport';
import { Log } from './utils/log';
import { MaglevState } from './maglev_state';
import { RelayTransport } from './transport/relay_transport';

export class Router {
  public inbound: ChannelSender<TransportMessage>;
  private inboundReceiver: ChannelReceiver<TransportMessage>;

  public outbound: ChannelSender<TransportMessage>;
  private outboundReceiver: ChannelReceiver<TransportMessage>;

  private log: Log;

  private peerTransports = new Map<string, Transport[]>();

  constructor(private nodeId: string, private maglevState: MaglevState) {
    [this.inbound, this.inboundReceiver] = channel();
    [this.outbound, this.outboundReceiver] = channel();
    this.log = maglevState.log.child('router');
    void this.inboundLoop();
    void this.outboundLoop();
  }

  public registerPeerTransports(peerNodeId: string, transports: Transport[]) {
    this.peerTransports.set(peerNodeId, transports);
    for (const transport of transports) {
      forward(transport.ioChannel[1], this.inbound);
    }
  }

  // Inbound

  private async inboundLoop() {
    for await (const { msg, err } of this.inboundReceiver) {
      if (err || !msg) {
        this.log.error('Unexpected inbound channel error:', err ?? 'no msg');
        throw err;
      }
      const srcNodeId = msg.header.srcNodeId;
      if (srcNodeId) {
        void this.dispatchInbound(srcNodeId, msg);
      } else {
        this.log.error('no srcNodeId on inbound message:', msg.header);
      }
    }
  }

  private async dispatchInbound(peerNodeId: string, msg: TransportMessage) {
    const peerState = this.maglevState.getPeerState(peerNodeId);

    // Transport control messages are handled by Transports.
    if (isTransportControlMsg(msg)) {
      const transports = await peerState.transports.get();
      for (const transport of transports) {
        // HACK: Currently this logic only applies to RelayTransports so this
        // works, but it might be more robust if RelayTransports registered
        // themselves as "children" of the relay and were routed that way.
        if (transport instanceof RelayTransport) {
          await transport.handleControlMsg(msg);
          return;
        }
      }
      console.error('No RelayTransport to handle routed control message:', msg);
      return;
    }

    // All other messages are dispatched to Sessions
    const session = await peerState.session.get();
    session.processMessage(msg);
  }

  // Outbound

  private async outboundLoop() {
    for await (const { msg, err } of this.outboundReceiver) {
      if (err || !msg) {
        this.log.error('Unexpected outbound channel error:', err ?? 'no msg');
        throw err;
      }
      const destNodeId = msg.header.destNodeId;
      if (destNodeId) {
        void this.dispatchOutbound(destNodeId, msg);
      } else {
        this.log.error('no destNodeId on outbound message:', msg.header);
      }
    }
  }

  private async dispatchOutbound(peerNodeId: string, msg: TransportMessage) {
    const transports = this.peerTransports.get(peerNodeId);
    if (!transports) {
      return;
    }
    for (const transport of transports) {
      if (transport.connected.get()) {
        transport.ioChannel[0].send(msg);
        break;
      }
    }
  }
}
