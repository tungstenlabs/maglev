// FIXME: would be nice for this to show fields from defaultMaglevConfig as
// not-null

export interface MaglevConfig {
  /**
   * The default value for session timeouts (can also be overridden on a session
   * by session basis) in milliseconds.
   */
  defaultSessionTimeoutMs?: number;

  relay: {
    url: string;
  };

  wrtc?: WebRtcConfig;
}

export interface WebRtcConfig {
  iceServers: RTCIceServer[];
}

export const defaultMaglevConfig: Partial<MaglevConfig> = {
  defaultSessionTimeoutMs: 10_000,
  wrtc: { iceServers: [] },
};
