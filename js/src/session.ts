import Long from 'long';
import { HandshakeResults } from './auth/handshake';
import { PeerAuth, PeerAuthInfo } from './auth/peer_auth';
import { channel } from './channels/buffered_channel';
import { ChannelSender } from './channels/channel_interfaces';
import { Exchange, OrderedReliable, ReliabilityType } from './exchange';
import {
  Code,
  errorMessage,
  isErrorMessage,
  StatusError,
} from './transport/errors';
import { TransportMessage } from './transport/transport';
import { BufferedEvent } from './utils/buffered_event';
import { maglevLog } from './utils/log';
import { LongMap } from './utils/long_map';
import { isZero } from './utils/number';

export interface IncomingExchange {
  readonly exchangeType: string;
  readonly peerAuthInfo: PeerAuthInfo;
  accept(reliabilityType?: ReliabilityType): Exchange;
}

export type IncomingExchangeCallback = (_: IncomingExchange) => Promise<void>;

export const EXCHANGE_CLOSED = new StatusError(
  Code.FailedPrecondition,
  'exchange closed'
);
const EXCHANGE_REJECTED = new StatusError(Code.Internal, 'exchange rejected');
const SESSION_RESET = new StatusError(Code.Aborted, 'session reset');

export class Session {
  /** Dispatched when the session is initialized or reset */
  public readonly newSessionEvent = new BufferedEvent<{
    peerAuthInfo: PeerAuthInfo;
  }>();

  private readonly firstExchangeId: number;

  private peerAuthInfo?: PeerAuthInfo;

  // Session state (cleared on session reset)
  public sessionId?: Long = undefined;
  private nextExchangeId: number;
  private readonly exchanges = new LongMap<Exchange>();
  private exchangesChannel = channel<TransportMessage>();

  constructor(
    public readonly peerNodeId: string,
    private readonly nodeId: string,
    private readonly peerAuth: PeerAuth,
    private readonly wireSender: ChannelSender<TransportMessage>,
    private readonly incomingExchangeCallback?: IncomingExchangeCallback,
    private readonly defaultReliabilityType = OrderedReliable
  ) {
    this.firstExchangeId = this.nodeId < peerNodeId ? 2 : 1;
    this.nextExchangeId = this.firstExchangeId;
  }

  public async handleHandshakeResults(handshakeResults: HandshakeResults) {
    const peerAuthInfo: PeerAuthInfo = {
      nodeId: this.peerNodeId,
      publicKey: handshakeResults.peerPublicKey,
      credential: handshakeResults.peerCredential,
    };
    try {
      void this.peerAuth.checkPeerAuth(peerAuthInfo);
    } catch (err) {
      // TODO: log err
      this.peerAuthInfo = undefined;
      this.reset();
      return;
    }
    this.peerAuthInfo = peerAuthInfo;

    // TODO: force session reset if authz doesn't match current (?)

    const sessionId = handshakeResults.sessionId;
    if (!this.sessionId?.equals(sessionId)) {
      const shouldReset = !!this.sessionId;

      this.sessionId = sessionId;

      if (shouldReset) {
        this.reset();
      }

      void this.outboundLoop();
      // Inform connection/transports
      this.newSessionEvent.dispatchEvent({ peerAuthInfo });
    }
  }

  public reset() {
    for (const [_, exchange] of this.exchanges) {
      exchange.close(SESSION_RESET);
    }
    this.exchanges.clear();
    this.exchangesChannel[1].fail(SESSION_RESET);
    this.exchangesChannel = channel();
    this.nextExchangeId = this.firstExchangeId;
  }

  public processMessage(msg: TransportMessage) {
    const exchangeId = msg.header.exchangeId;
    if (!exchangeId || isZero(exchangeId)) {
      console.warn('Session.processMessage: got exchangeId 0');
      return;
    }

    const exchange = this.exchanges.get(exchangeId);

    if (exchange) {
      exchange.processMessage(msg);
    } else if (msg.header.exchangeType) {
      void this.handleIncomingExchange(msg);
    } else if (isErrorMessage(msg)) {
      console.debug('Got error on closed exchange:', msg.header.error);
    } else {
      const errMsg = errorMessage(EXCHANGE_CLOSED, { exchangeId });
      this.sendMessage(errMsg).catch((err) => {
        console.error('Failed sending exchange error message:', err);
      });
    }
  }

  public createExchange(
    exchangeType: string,
    reliabilityType: ReliabilityType = this.defaultReliabilityType
  ): Exchange {
    const exchangeId = Long.fromNumber(this.nextExchangeId, true);
    this.nextExchangeId += 2;
    return this.initExchange(exchangeId, exchangeType, true, reliabilityType);
  }

  private async handleIncomingExchange(msg: TransportMessage) {
    try {
      // eslint-disable-next-line @typescript-eslint/no-this-alias
      const session = this;
      const { exchangeId, exchangeType } = msg.header;
      let accepted: boolean | undefined;

      if (!this.peerAuthInfo) {
        const authError = new StatusError(Code.Unauthenticated);
        await this.sendMessage(errorMessage(authError, { exchangeId }));
        return;
      }

      // Construct callback argument
      const incomingExchange: IncomingExchange = {
        exchangeType: exchangeType!,
        peerAuthInfo: this.peerAuthInfo,
        accept(
          reliabilityType: ReliabilityType = session.defaultReliabilityType
        ): Exchange {
          const exchange = session.initExchange(
            exchangeId! instanceof Long
              ? exchangeId!
              : Long.fromNumber(exchangeId!, true),
            exchangeType!,
            false,
            reliabilityType
          );
          exchange.processMessage(msg);
          accepted = true;
          return exchange;
        },
      };

      // Call callback
      let rejectErr: Error | undefined;
      try {
        await this.incomingExchangeCallback?.(incomingExchange);
      } catch (err) {
        rejectErr = err;
      }

      // If the callback didn't accept, reject
      if (!accepted) {
        rejectErr ??= EXCHANGE_REJECTED;
        await this.sendMessage(errorMessage(rejectErr, { exchangeId }));
      }
    } catch (err) {
      console.error('Session handleIncomingExchange error:', err);
    }
  }

  private outboundLoopStarted = false;

  private async outboundLoop() {
    if (this.outboundLoopStarted) {
      return;
    }
    this.outboundLoopStarted = true;
    try {
      for await (const { msg, err } of this.exchangesChannel[1]) {
        if (err || !msg) {
          if (err !== SESSION_RESET) {
            maglevLog.child('session').error(err);
          }
          break;
        }
        await this.sendMessage(msg);
      }
    } catch (err) {
      maglevLog.child('session').error(err);
      // TODO: close session?
    }
  }

  private async sendMessage(msg: TransportMessage) {
    msg.header = {
      ...msg.header,
      srcNodeId: this.nodeId,
      destNodeId: this.peerNodeId,
      sessionId: this.sessionId,
    };
    this.wireSender.send(msg);
  }

  private initExchange(
    exchangeId: Long,
    exchangeType: string,
    initiating: boolean,
    reliabilityType: ReliabilityType
  ): Exchange {
    const exchange = new Exchange(
      exchangeId,
      exchangeType,
      initiating,
      reliabilityType,
      this.exchangesChannel[0]
    );
    this.exchanges.set(exchangeId, exchange);
    exchange.exchangeCompleteEvent.addHandler(() => {
      if (Object.is(this.exchanges.get(exchangeId), exchange)) {
        this.exchanges.delete(exchangeId);
      }
    });
    return exchange;
  }
}
