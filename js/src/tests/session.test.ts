import Long from 'long';
import { FingerprintIdentityPeerAuth } from '../auth/fingerprint_identity';
import { channel } from '../channels/buffered_channel';
import { ChannelReceiver, ChannelSender } from '../channels/channel_interfaces';
import { takeOne } from '../channels/take_one';
import { Exchange } from '../exchange';
import { IncomingExchange, Session } from '../session';
import { TransportMessage } from '../transport/transport';
import './lib/jest_matchers';
import { TEST_NODE_A, TEST_NODE_B } from './lib/test_node_data';

const testSessionId = Long.fromNumber(12345);

let wireReceiver: ChannelReceiver<TransportMessage>;
let session: Session;

async function mockHandshake(sessionId: Long = testSessionId) {
  await session.handleHandshakeResults({
    peerPublicKey: TEST_NODE_B.keyPair.publicKey,
    sessionId,
  });
}

let incomingExchange: Exchange;
async function incomingExchangeHandler(incoming: IncomingExchange) {
  if (incoming.exchangeType === 'rejectMe') {
    throw 'I reject you!';
  }
  incomingExchange = incoming.accept();
}

beforeEach(() => {
  let wireSender: ChannelSender<TransportMessage>;
  [wireSender, wireReceiver] = channel<TransportMessage>();
  session = new Session(
    TEST_NODE_B.nodeId,
    TEST_NODE_A.nodeId,
    new FingerprintIdentityPeerAuth(TEST_NODE_B.nodeId),
    wireSender,
    incomingExchangeHandler
  );
});

test('session init', async () => {
  const mockNewSessionEventHandler = jest.fn();
  session.newSessionEvent.addHandler(mockNewSessionEventHandler);
  await mockHandshake();
  expect(mockNewSessionEventHandler).toHaveBeenCalledTimes(1);
  expect(mockNewSessionEventHandler.mock.calls[0][0]).toEqual({
    peerAuthInfo: expect.objectContaining({
      nodeId: TEST_NODE_B.nodeId,
    }),
  });
  expect(session.sessionId).toEqualLong(testSessionId);
});

test('createExchange', async () => {
  await mockHandshake();

  const exchange = session.createExchange('testType');
  exchange.channel[0].open();

  expect(await takeOne(wireReceiver)).toMatchObject({
    header: {
      exchangeType: 'testType',
      sessionId: expect.toEqualLong(session.sessionId!),
      exchangeId: expect.toEqualLong(exchange.exchangeId),
    },
  });
});

test('incomingExchange', async () => {
  await mockHandshake();

  const exchangeId = Long.fromNumber(54321);
  session.processMessage({
    header: {
      exchangeId: exchangeId,
      exchangeType: 'incomingExchangeType',
      noPayload: true,
    },
  });

  expect(incomingExchange).toMatchObject({
    exchangeId: expect.toEqualLong(exchangeId),
    exchangeType: 'incomingExchangeType',
  });
});

test('incomingExchange reject', async () => {
  await mockHandshake();

  session.processMessage({
    header: {
      exchangeId: Long.ONE,
      exchangeType: 'rejectMe',
      noPayload: true,
    },
  });

  const errMsg = await takeOne(wireReceiver);
  expect(errMsg.header.error?.message).toMatch('reject you');
});

test('session reset', async () => {
  const mockNewSessionEventHandler = jest.fn();
  session.newSessionEvent.addHandler(mockNewSessionEventHandler);

  await mockHandshake();

  const exchange = session.createExchange('exchangeType');

  const mockExchangeClosedEventHandler = jest.fn();
  exchange.exchangeCompleteEvent.addHandler(mockExchangeClosedEventHandler);

  await mockHandshake(Long.ONE);
  expect(mockNewSessionEventHandler).toHaveBeenCalledTimes(2);

  expect(exchange.channel[1].tryReceiveNow()!.err).toBeTruthy();
  expect(mockExchangeClosedEventHandler).toHaveBeenCalled();
});

test('exchange closed', async () => {
  await mockHandshake();

  session.processMessage({
    header: {
      exchangeId: Long.ONE,
      exchangeSeq: Long.ONE,
      noPayload: true,
    },
  });

  const errMsg = await takeOne(wireReceiver);
  expect(errMsg.header.error?.message).toMatch('exchange closed');
});
