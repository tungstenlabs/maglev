import Long from 'long';
import { Handshake } from '../auth/handshake';
import { bytesToString } from '../utils/string';
import { SIGNING_PAIR_A, SIGNING_PAIR_B } from './lib/test_crypto_keys';

const keyPairA = SIGNING_PAIR_A;
const keyPairB = SIGNING_PAIR_B;

test('one side initiated handshake, new session', async () => {
  const handshakeA = new Handshake(keyPairA);
  const handshakeB = new Handshake(keyPairB);

  const helloA = await handshakeA.getHello();
  const respB = await handshakeB.processHello(helloA!);

  // Once it has processed a Hello, don't generate a new one
  const helloBnull = await handshakeB.getHello();
  expect(helloBnull).toBeNull();

  const [resultA, respA] = await handshakeA.processHelloResponse(respB);
  const [resultB, undef] = await handshakeB.processHelloResponse(respA!);

  // Final processHelloResponse doesn't return a HelloResponse.
  expect(undef).toBeUndefined();

  expect(resultA.sessionId.isZero()).toBe(false);
  expect(resultA.sessionId).toEqual(resultB.sessionId);

  expect(await resultA.peerPublicKey.equals(keyPairB.publicKey)).toBe(true);
  expect(await resultB.peerPublicKey.equals(keyPairA.publicKey)).toBe(true);

  expect(resultA.peerCredential).toBeUndefined();
  expect(resultB.peerCredential).toBeUndefined();
});

test('symmetric handshake, resume session', async () => {
  const sessionId = new Long(123);
  const handshakeA = new Handshake(keyPairA, 'tokenA', sessionId);
  const handshakeB = new Handshake(keyPairB, 'tokenB', sessionId);

  const helloA = await handshakeA.getHello();
  const helloB = await handshakeB.getHello();
  const respA = await handshakeA.processHello(helloB!);
  const respB = await handshakeB.processHello(helloA!);
  const [resultA, undefA] = await handshakeA.processHelloResponse(respB!);
  const [resultB, undefB] = await handshakeB.processHelloResponse(respA!);

  expect(undefA).toBeUndefined();
  expect(undefB).toBeUndefined();

  expect(resultA.sessionId).toEqual(sessionId);
  expect(resultB.sessionId).toEqual(sessionId);

  expect(bytesToString(resultA.peerCredential!)).toEqual('tokenB');
  expect(bytesToString(resultB.peerCredential!)).toEqual('tokenA');
});
