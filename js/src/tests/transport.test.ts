import Long from 'long';
import { Channel } from '../channels/channel_interfaces';
import { Transport, TransportMessage } from '../transport/transport';
import { AsyncValue } from '../utils/async_value';
import { sleep } from '../utils/time';
import { TestNode, TEST_NODE_A, TEST_NODE_B } from './lib/test_node_data';

jest.setTimeout(1000);

class TestTransport extends Transport {
  constructor(private testNode: TestNode, peerTestNode: TestNode) {
    super('TestTransport', peerTestNode.nodeId, testNode.authContext, {
      sessionId: new AsyncValue(),
    });
    Transport.RETRY_TIMER_MS = 2;
  }

  protected newHandshake() {
    return this.testNode.getHandshake();
  }

  public attemptReconnection = jest.fn();
  public beginLifecycle = super.beginLifecycle;
}

const thisNode = TEST_NODE_A;
const peerNode = TEST_NODE_B;

const newTestTransport = () => new TestTransport(thisNode, peerNode);

test('create transport instance', () => {
  expect(newTestTransport()).toBeTruthy();
});

test('attempt reconnect happy path', async () => {
  const transport: any = newTestTransport();
  const handshake = jest
    .spyOn(transport, 'startHandshake')
    .mockImplementation(async () => {
      transport.connected.set(true);
    });
  transport.attemptReconnection.mockImplementationOnce(async () => {});
  void transport.beginLifecycle();
  await sleep(1);

  expect(transport.attemptReconnection).toBeCalled();
  expect(handshake).toBeCalled();
  expect(transport.connected).toBeTruthy();
});

test('attempt reconnect retries after RETRY_TIMER_MS', async () => {
  const transport: any = newTestTransport();
  const handshake = jest
    .spyOn(transport, 'startHandshake')
    .mockImplementation(async () => {
      transport.connected.set(true);
    });
  transport.attemptReconnection.mockImplementationOnce(async () => {
    throw new Error('TEST ERROR');
  });
  void transport.beginLifecycle();

  await sleep(1);
  expect(transport.attemptReconnection).toBeCalled();
  expect(handshake).not.toBeCalled();
  expect(transport.connected.get()).toBeFalsy();

  // Reset the mock to check for the timeout retrying.
  transport.attemptReconnection.mockClear();
  transport.attemptReconnection.mockImplementationOnce(async () => {});

  await sleep(3);
  expect(transport.attemptReconnection).toBeCalled();
  expect(handshake).toBeCalled();
});

test('handshake await hello', async () => {
  const transport = newTestTransport();
  transport.attemptReconnection.mockImplementationOnce(async () => {});
  const [sender, receiver] = (transport as any)
    .internalIOChannel as Channel<TransportMessage>;
  void transport.beginLifecycle();

  // Wait for a Hello message
  await receiver.awaitPeekFirst();
  expect(receiver.tryReceiveNow()!.msg).toMatchObject({
    header: { exchangeType: 'Hello' },
  });

  // Send a Hello
  peerNode.sendHello(sender);

  // Await the response
  await receiver.awaitPeekFirst();
  expect(receiver.tryReceiveNow()!.msg).toMatchObject({
    header: { exchangeType: 'HelloResponse' },
  });

  // Send our own response
  peerNode.sendHelloResponse(sender);

  // Make sure it doesn't puke on sending a Hello after this
  peerNode.sendHello(sender);

  await sleep(1);

  // Should be healthy at this point.
  expect(transport.connected.get()).toBeTruthy();
});

test('handshake send hello first', async () => {
  const transport = newTestTransport();
  transport.attemptReconnection.mockImplementationOnce(async () => {});
  const [sender, receiver] = (transport as any)
    .internalIOChannel as Channel<TransportMessage>;
  // Send a Hello first
  peerNode.sendHello(sender);

  void transport.beginLifecycle();

  await sleep(1);

  // Await the hello and hello response
  await receiver.awaitPeekFirst();
  expect(receiver.tryReceiveNow()!.msg).toMatchObject({
    header: { exchangeType: 'Hello' },
  });

  await receiver.awaitPeekFirst();
  expect(receiver.tryReceiveNow()!.msg).toMatchObject({
    header: { exchangeType: 'HelloResponse' },
  });

  // Send our own response
  peerNode.sendHelloResponse(sender);

  await sleep(1);

  // Should be healthy at this point.
  expect(transport.connected).toBeTruthy();
});

test('dispose', async () => {
  const transport = newTestTransport();
  transport.dispose();

  // Call it again to make sure it doesn't explode.
  transport.dispose();
});

test('encode decode', async () => {
  const transport: any = newTestTransport();
  const msg: TransportMessage = {
    header: {
      // Just some random stuff
      sessionId: Long.fromNumber(42, true),
      exchangeContinues: true,
      exchangeType: 'ENCODE_DECODE_TEST_HEADER',
    },
    payload: new Uint8Array([1, 2, 3]),
  };
  const bytes = transport.encodePrefixedMaglevMsg(msg);
  const copy = transport.decodePrefixedMaglevMsg(bytes);
  expect(copy).toMatchObject(msg);
});
