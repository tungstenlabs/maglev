import {
  lazyObservable,
  mapObservable,
  ObservableValue,
  reduceObservables,
} from '../utils/observable';
import { sleep } from '../utils/time';
import { TimeoutError } from '../utils/timeout';

// This resolves after any other Promises that would be resolved this tick
function promiseResolution(): Promise<void> {
  return new Promise(setImmediate);
}

test('get returns current value', async () => {
  const obs = new ObservableValue(1);
  expect(obs.get()).toBe(1);

  obs.set(2);
  expect(obs.get()).toBe(2);
});

test('subscribe gets updates', async () => {
  const obs = new ObservableValue(1);
  const handler = jest.fn();

  obs.subscribe(handler);
  expect(handler).not.toHaveBeenCalled();

  obs.set(2);
  expect(handler).toHaveBeenCalledWith(2);

  obs.set(3);
  expect(handler).toHaveBeenCalledWith(3);
});

test('consecutive updates with the same value are ignored', async () => {
  const obs = new ObservableValue(1);
  const handler = jest.fn();

  obs.subscribe(handler);
  obs.set(1);
  expect(handler).not.toHaveBeenCalled();
});

test('observe calls the handler with the current value and subscribes', async () => {
  const obs = new ObservableValue(1);
  const handler = jest.fn();

  obs.observe(handler);
  expect(handler).toHaveBeenCalledWith(1);

  obs.set(2);
  expect(handler).toHaveBeenCalledWith(2);
});

test('isObserved returns false if nothing is subscribed', async () => {
  const obs = new ObservableValue(1);
  obs.get();
  expect(obs.isObserved).toBe(false);
});

test('isObserved returns true if something is subscribed', async () => {
  const obs = new ObservableValue(1);
  obs.subscribe(() => {});
  expect(obs.isObserved).toBe(true);
});

test('subscribe cancel works', async () => {
  const obs = new ObservableValue(1);
  const handler = jest.fn();
  const cancel = obs.subscribe(handler);

  obs.set(2);
  expect(handler).toHaveBeenCalledWith(2);
  handler.mockClear();

  cancel();
  obs.set(3);
  expect(handler).not.toHaveBeenCalled();
  expect(obs.isObserved).toBe(false);
});

test('when resolves immediately if the condition is already met', async () => {
  const obs = new ObservableValue(1);
  void expect(obs.when((value) => value < 5)).resolves.toBe(1);
});

test('when resolves when the condition is met', async () => {
  const obs = new ObservableValue(1);
  const callback = jest.fn();
  const finished = obs.when((value) => value > 5).then(callback);

  obs.set(2);
  await promiseResolution();
  expect(callback).not.toHaveBeenCalled();

  obs.set(8);
  await promiseResolution();
  expect(callback).toHaveBeenCalledWith(8);
});

test('when rejects immediately if the condition function throws immediately', async () => {
  const obs = new ObservableValue(1);
  const err = new Error();
  void expect(
    obs.when(() => {
      throw err;
    })
  ).rejects.toBe(err);
});

test('when rejects later if the condition function throws later', async () => {
  const obs = new ObservableValue(1);
  const err = new Error();
  const expectPromise = expect(
    obs.when((value) => {
      if (value > 1) {
        throw err;
      }
      return false;
    })
  ).rejects.toBe(err);
  obs.set(2);
  return expectPromise;
});

test('when with timeout resolves if the condition is met in time', async () => {
  const obs = new ObservableValue(1);
  const expectPromise = expect(
    obs.when((value) => value > 1, 1000)
  ).resolves.toBe(2);
  await sleep(1);
  obs.set(2);
  return expectPromise;
});

test('when with timeout rejects if the condition is not met', async () => {
  const obs = new ObservableValue(1);
  const expectPromise = expect(
    obs.when((value) => value > 3, 10)
  ).rejects.toBeInstanceOf(TimeoutError);
  obs.set(2);
  return expectPromise;
});

test('whenEqual resolves to true when the value matches', async () => {
  const obs = new ObservableValue(1);
  return expect(obs.whenEqual(1)).resolves.toBe(true);
});

test('whenEqual resolves to false if it times out', async () => {
  const obs = new ObservableValue(1);
  return expect(obs.whenEqual(2, 1)).resolves.toBe(false);
});

test('lazyObservable waits to be observed before starting', async () => {
  const initialValue = jest.fn(() => 1);
  const builder = jest.fn(async (val) => new ObservableValue(val));
  const obs = lazyObservable(initialValue, builder);

  await promiseResolution();
  expect(initialValue).not.toHaveBeenCalled();
  expect(builder).not.toHaveBeenCalled();

  expect(obs.get()).toBe(1);
  await promiseResolution();
  expect(initialValue).toHaveBeenCalled();
  expect(builder).toHaveBeenCalled();
});

test('lazyObservable updates if inner Observable initialized with different value', async () => {
  const obs = lazyObservable(
    () => 1,
    async () => new ObservableValue(2)
  );
  const handler = jest.fn();
  expect(obs.get()).toBe(1);

  obs.subscribe(handler);
  await promiseResolution();
  expect(handler).toHaveBeenCalledWith(2);
});

test('reduceObservable reduces input Observables to output', async () => {
  const obs1 = new ObservableValue(2);
  const obs2 = new ObservableValue(3);
  const obs = reduceObservables([obs1, obs2], ([v1, v2]) => `${v1},${v2}`);

  expect(obs.get()).toBe('2,3');

  const handler = jest.fn();
  obs.subscribe(handler);

  obs1.set(5);
  expect(handler).toHaveBeenLastCalledWith('5,3');

  obs2.set(7);
  expect(handler).toHaveBeenLastCalledWith('5,7');
});

test('mapObservable maps Observables', async () => {
  const obs1 = new ObservableValue(2);
  const obs2 = new ObservableValue('x');
  const obs = mapObservable(obs1, obs2, (num, str) => str.repeat(num));
  expect(obs.get()).toBe('xx');

  obs1.set(3);
  expect(obs.get()).toBe('xxx');

  obs2.set('y');
  expect(obs.get()).toBe('yyy');
});
