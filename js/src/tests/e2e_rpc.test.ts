import { channel, ChannelReceiver, Maglev } from '..';
import { Transport } from '../transport/transport';
import { OrderedReliable } from '../exchange';
import { wlabs } from './gen/proto_gen';
import Long from 'long';

type PingResponse = wlabs.maglev.core.test.coverage.PingResponse;
type ComplexType = wlabs.maglev.core.test.coverage.ComplexType;

const complexType: ComplexType = {
  doubleValue: 42.25,
  floatValue: 12.25,
  int32Type: 1,
  int64Type: Long.fromNumber(2, false),
  uint32Type: 3,
  uint64Type: Long.fromNumber(4, true),
  sint32Type: 5,
  sint64Type: Long.fromNumber(6, false),
  fixed32Type: 7,
  fixed64Type: Long.fromNumber(8, true),
  sfixed32Type: 9,
  sfixed64Type: Long.fromNumber(10, false),
  boolType: true,
  stringType: 'string-type',
  // bytesField: new Uint8Array([1, 2, 3, 4]),
  embeddedType: {
    embeddedFieldOne: 11,
    embeddedFieldTwo: 'embedded-string',
  },
  embeddedEnumType:
    wlabs.maglev.core.test.coverage.ComplexType.EmbeddedEnum.SECOND,
  oneofString: 'one-of-string-set',
  repeatedInt64: [
    Long.fromNumber(12, false),
    Long.fromNumber(13, false),
    Long.fromNumber(14, false),
  ],
};

beforeEach(() => {
  // Lower the handshake timeout
  (Transport as any).HANDSHAKE_TIMEOUT_MS = 200;
  (OrderedReliable as any).INITIAL_RETRANSMIT_TIMEOUT = 2;
});

test('rpc coverage test', async () => {
  const maglev = new Maglev({
    relay: { url: 'fake' },
  });

  // Handler
  wlabs.maglev.core.test.coverage.RpcCoverageService.registerHandler(maglev, {
    ping: async function (req, _ctx) {
      return { senderTime: req.senderTime, count: 0 };
    },
    streamToHost: async function (req, _ctx) {
      // Count how many are send and send back the count
      let count = 0;
      for await (const res of req) {
        if (res.msg) {
          count++;
        }
      }
      return { count };
    },
    streamFromHostChannel: function (req, _ctx) {
      return nChannel(req.count);
    },
    streamFromHostAsyncGen: async function* (req, _ctx) {
      for (let i = 0; i < req.count; i++) {
        yield { count: i };
      }
    },
    streamBothChannel: async function (req, _ctx) {
      const c = await countChannel(req);
      return nChannel(c);
    },
    streamBothAsyncGen: async function* (req, _ctx) {
      for await (const res of req) {
        if (res.msg) {
          yield { count: 1 };
        }
      }
    },
    typeTest: async function (req, _ctx) {
      expect(req).toMatchObject(complexType);
      return { ...complexType };
    },
  });

  // Test RPC types
  const selfRef = await maglev.nodes.self();
  const client = wlabs.maglev.core.test.coverage.RpcCoverageService.createClient(
    selfRef
  );

  expect((await client.streamToHost(nChannel(10))).count).toEqual(10);
  expect(
    await countChannel(client.streamFromHostChannel({ count: 10 }))
  ).toEqual(10);
  expect(
    await countChannel(client.streamFromHostAsyncGen({ count: 10 }))
  ).toEqual(10);
  expect(await countChannel(client.streamBothChannel(nChannel(10)))).toEqual(
    10
  );
  expect(await countChannel(client.streamBothAsyncGen(nChannel(10)))).toEqual(
    10
  );
  expect(await client.typeTest({ ...complexType })).toMatchObject(complexType);
});

function nChannel(n: number) {
  const [sender, receiver] = channel<any>();
  for (let i = 0; i < n; i++) {
    sender.send({ count: i });
  }
  sender.close();
  return receiver;
}

async function countChannel(receiver: ChannelReceiver<PingResponse>) {
  let i = 0;
  for await (const { msg, err } of receiver) {
    if (err || !msg) {
      throw err || 'no msg';
    }
    i++;
  }
  return i;
}
