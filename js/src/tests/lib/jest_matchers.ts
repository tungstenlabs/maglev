import Long from 'long';

expect.extend({
  toEqualLong(received: any, value: Long | number) {
    const pass = !!received?.equals?.(value);
    return {
      pass,
      message: () =>
        `expected ${received}${pass ? ' not' : ''} to equal ${value}`,
    };
  },

  async toYield(received: any, ...expected: any[]) {
    const receivedValues: any[] = [];
    for await (const res of received) {
      if (res.msg) {
        receivedValues.push(res.msg);
      }
    }
    const pass = this.equals(receivedValues, expected, [
      this.utils.iterableEquality,
    ]);
    return {
      pass,
      message: () =>
        this.utils.matcherHint('toYield') +
        '\n\n' +
        this.utils.printDiffOrStringify(
          expected,
          receivedValues,
          'Expected',
          'Received',
          true
        ),
    };
  },
});

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace jest {
    interface Expect {
      toEqualLong(value: Long | number): any;
    }
    interface Matchers<R> {
      toEqualLong(value: Long | number): R;
      toYield(...values: any[]): Promise<R>;
    }
  }
}
