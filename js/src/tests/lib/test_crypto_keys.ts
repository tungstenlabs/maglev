import { crypto } from '../../cryptov1/webcrypto';
import {
  KeyPair,
  PrivateKey,
  PublicKey,
  WebCryptoPrivateKey,
  WebCryptoPublicKey,
} from '../../cryptov1/signing';

export class TestKey implements PrivateKey, PublicKey {
  constructor(
    public readonly name: string,
    private readonly pub: Promise<PublicKey>,
    private readonly priv?: Promise<PrivateKey>
  ) {}
  public async verifyParts(...args: [any, any, ...any[]]): Promise<boolean> {
    return (await this.pub).verifyParts(...args);
  }
  public async exportRaw(): Promise<Uint8Array> {
    return (await this.pub).exportRaw();
  }
  public async equals(other: any): Promise<boolean> {
    return (await this.pub).equals(other);
  }
  public async signParts(...args: [any, ...any[]]): Promise<Uint8Array> {
    if (!this.priv) {
      throw `no private key for ${this.name}`;
    }
    return (await this.priv).signParts(...args);
  }

  public static pair(name: string, jwk: JsonWebKey): KeyPair {
    const key = new TestKey(
      name,
      importKey(jwk, 'verify').then((key) => new WebCryptoPublicKey(key)),
      importKey(jwk, 'sign').then((key) => new WebCryptoPrivateKey(key))
    );
    return { privateKey: key, publicKey: key };
  }
}

function importKey(jwk: JsonWebKey, op: 'sign' | 'verify'): Promise<CryptoKey> {
  if (op === 'verify') {
    jwk = { ...jwk, d: undefined };
  }
  return crypto.subtle.importKey(
    'jwk',
    { ...jwk, kty: 'EC', key_ops: [op] },
    { name: 'ECDSA', namedCurve: 'P-256' },
    op === 'verify',
    [op]
  );
}

export const SIGNING_PAIR_A = TestKey.pair('A', {
  d: 'jLpu0juqi8F4wwhJeX5G8FPzjSzLg2RI1doTMKbW57o',
  x: '_7yyUsJf2t2Q7qemjtucuW8c1AOtaZ5BYwvbyoQn6kw',
  y: 'WIJon361sDKmqVmg1SuoNNw11QpORYUgkWI2N-7lpE0',
});
export const SIGNING_PAIR_B = TestKey.pair('B', {
  d: 'k6U_t2JLzBdoN_PaHR8t0uf_WQr19KiRIZy9pgMjUz4',
  x: 'XytZzb3xA-LrwuPc9khbeGPNxzNHjEQ6wseYMZ2pWao',
  y: '6ozhb9YpAz1Qje5MwDvmSzyGZx1kFzIXCnr-uEBh-gM',
});
export const SIGNING_PAIR_C = TestKey.pair('C', {
  d: 'u2tZcQ557nYJsNKYLJGQzZLdOy984KRRLfPN06ES4F4',
  x: 'VOjfQJoNzuUto0Q_f8RcC3zu7If0SOc99E78bUyXt0Y',
  y: 'KDGwUMOpTJUQxjMXIM0EQi2APwfoVrV0wmfmilf5LHo',
});
