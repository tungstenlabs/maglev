import Long from 'long';
import { wlabs } from '../..';
import { HandshakeIface } from '../../auth/handshake';
import { AuthContext } from '../../auth_context';
import { ChannelSender } from '../../channels/channel_interfaces';
import { KeyPair } from '../../cryptov1/signing';
import { TransportMessage } from '../../transport/transport';
import { toLong } from '../../utils/number';
import { stringToBytes } from '../../utils/string';
import { SIGNING_PAIR_A, SIGNING_PAIR_B } from './test_crypto_keys';

export class TestNode {
  public authContext: AuthContext;

  constructor(public nodeId: string, public keyPair: KeyPair) {
    this.authContext = {
      nodeId,
      keyPair,
    };
  }

  public getHandshake(): HandshakeIface {
    return {
      getHello: async () => ({
        version: 1,
        resumeSessionId: Long.ZERO,
        ephemeralPublicKey: stringToBytes('EPHEMERAL'),
      }),
      processHello: async (
        hello: wlabs.maglev.handshake.Hello_WithDefaultValues
      ) => ({
        sessionId: toLong(hello.resumeSessionId, true),
        ephemeralPublicKey: stringToBytes('EPHEMERAL'),
        publicKey: stringToBytes('PUBLIC KEY'),
        credential: this.authContext.credential ?? new Uint8Array(),
        signature: new Uint8Array(),
      }),
      processHelloResponse: async (
        resp: wlabs.maglev.handshake.HelloResponse_WithDefaultValues
      ) => [
        {
          sessionId: toLong(resp.sessionId, true),
          peerPublicKey: TEST_NODE_B.keyPair.publicKey,
        },
        undefined,
      ],
    };
  }

  public sendHello(sender: ChannelSender<TransportMessage>) {
    sender.send({
      header: { exchangeType: 'Hello' },
      payload: wlabs.maglev.handshake.Hello.encode({ version: 1 }),
    });
  }

  public sendHelloResponse(sender: ChannelSender<TransportMessage>) {
    sender.send({
      header: { exchangeType: 'HelloResponse' },
      payload: wlabs.maglev.handshake.HelloResponse.encode({
        sessionId: 1,
      }),
    });
  }
}

export const TEST_NODE_A = new TestNode('', SIGNING_PAIR_A);
export const TEST_NODE_B = new TestNode(
  'sywlbhy33j6jis7r4lsie5s4c4',
  SIGNING_PAIR_B
);
