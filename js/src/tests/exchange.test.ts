import Long from 'long';
import { channel } from '../channels/buffered_channel';
import { ChannelReceiver } from '../channels/channel_interfaces';
import {
  Exchange,
  OrderedReliable,
  ReliabilityType,
  UnorderedUnreliable,
} from '../exchange';
import { EXCHANGE_CLOSED } from '../session';
import { Code, errorMessage, StatusError } from '../transport/errors';
import { TransportMessage } from '../transport/transport';
import './lib/jest_matchers';

type MsgHeader = TransportMessage['header'];

const testExchangeId = Long.fromNumber(31415926535);
const testExchangeType = 'testExchangeType';

function testMessage(
  header: MsgHeader,
  payload?: Uint8Array | number[]
): TransportMessage {
  const msg: TransportMessage = {
    header: {
      exchangeId: testExchangeId,
      noPayload: !payload,
      ...header,
    },
  };
  if (payload) {
    msg.payload =
      payload instanceof Uint8Array ? payload : new Uint8Array(payload);
  }
  return msg;
}

function testExchange(
  initiating: boolean,
  reliabilityType: ReliabilityType = OrderedReliable
): [Exchange, ChannelReceiver<TransportMessage>] {
  const [wireSender, wireReceiver] = channel<TransportMessage>();
  const exchange = new Exchange(
    testExchangeId,
    testExchangeType,
    initiating,
    reliabilityType,
    wireSender
  );
  return [exchange, wireReceiver];
}

async function nextMsg<T>(channel: ChannelReceiver<T>): Promise<T | null> {
  await channel.awaitPeekFirst();
  const res = channel.tryReceiveNow();
  if (!res) {
    throw new Error('No message');
  }
  return res.msg ?? null;
}

jest.setTimeout(1000);

function sharedReliabilityTests(reliabilityType: ReliabilityType) {
  test('unary send', async () => {
    const [exchange, wireReceiver] = testExchange(true, reliabilityType);

    const payload = new Uint8Array([1, 2, 3, 4, 5]);
    exchange.channel[0].sendAndClose(new Uint8Array(payload));

    expect(await nextMsg(wireReceiver)).toMatchObject<TransportMessage>({
      header: {
        exchangeId: expect.toEqualLong(testExchangeId),
        exchangeType: testExchangeType,
        exchangeContinues: false,
        noPayload: false,
      },
      payload,
    });
  });

  test('stream send', async () => {
    const [exchange, wireReceiver] = testExchange(true, reliabilityType);
    const payloadSender = exchange.channel[0];

    payloadSender.open();
    expect(await nextMsg(wireReceiver)).toMatchObject<TransportMessage>({
      header: {
        exchangeId: expect.toEqualLong(testExchangeId),
        exchangeSeq: expect.toEqualLong(0),
        exchangeType: testExchangeType,
        exchangeContinues: true,
        noPayload: true,
      },
      payload: undefined,
    });

    const payload = new Uint8Array([1, 2, 3, 4, 5]);
    payloadSender.send(payload);
    expect(await nextMsg(wireReceiver)).toMatchObject<TransportMessage>({
      header: {
        exchangeId: expect.toEqualLong(testExchangeId),
        exchangeSeq: expect.toEqualLong(1),
        exchangeContinues: true,
        noPayload: false,
      },
      payload,
    });

    payloadSender.close();
    expect(await nextMsg(wireReceiver)).toMatchObject<TransportMessage>({
      header: {
        exchangeId: expect.toEqualLong(testExchangeId),
        exchangeSeq: expect.toEqualLong(2),
        exchangeContinues: false,
        noPayload: true,
      },
    });
  });

  test('error send', async () => {
    const [exchange, wireReceiver] = testExchange(true, reliabilityType);

    exchange.channel[0].open();
    expect(await nextMsg(wireReceiver)).toBeTruthy();

    exchange.channel[0].fail(new StatusError(Code.OutOfRange, 'test error'));
    expect(await nextMsg(wireReceiver)).toMatchObject<TransportMessage>({
      header: {
        exchangeId: expect.toEqualLong(testExchangeId),
        error: {
          code: Code.OutOfRange,
          message: 'test error',
        },
        noPayload: true,
      },
    });
  });

  test('unary receive', async () => {
    const [exchange, _] = testExchange(false, reliabilityType);

    const payload = new Uint8Array([1, 2, 3, 4, 5]);
    exchange.processMessage(testMessage({}, payload));

    await expect(exchange.channel[1]).toYield(payload);
  });

  test('stream receive', async () => {
    const [exchange, _] = testExchange(false, reliabilityType);

    const payload = new Uint8Array([1, 2, 3, 4, 5]);
    exchange.processMessage(
      testMessage({ exchangeSeq: Long.ZERO, exchangeContinues: true })
    );
    exchange.processMessage(
      testMessage({ exchangeSeq: Long.ONE, exchangeContinues: true }, payload)
    );
    exchange.processMessage(testMessage({ exchangeSeq: Long.fromNumber(2) }));

    await expect(exchange.channel[1]).toYield(payload);
  });

  test('error receive', async () => {
    const [exchange, _] = testExchange(false, reliabilityType);

    const err = new StatusError(Code.OutOfRange, 'test error');
    exchange.processMessage(errorMessage(err));

    await exchange.channel[1].awaitPeekFirst();
    expect(exchange.channel[1].tryReceiveNow()!.err).toEqual(err);
  });

  test('duplicate suppression', async () => {
    const [exchange, _] = testExchange(false, reliabilityType);

    const payload = new Uint8Array([1, 1, 1]);
    const msg = testMessage({}, payload);

    exchange.processMessage(msg);
    exchange.processMessage(msg);

    await expect(exchange.channel[1]).toYield(payload);
  });
}

describe(OrderedReliable, () => {
  sharedReliabilityTests(OrderedReliable);

  test('closing', async () => {
    const [exchange, wireReceiver] = testExchange(false);

    // Close inbound (half closed)
    exchange.processMessage(testMessage({}));
    expect(exchange.channel[1].tryReceiveNow()).toBeNull();

    // Close outbound
    exchange.channel[0].close();
    const outboundCloseMsg = await nextMsg(wireReceiver);
    expect(outboundCloseMsg!.header.exchangeContinues).toBeFalsy();

    // ACK outbound close
    exchange.processMessage(
      testMessage({
        expectingExchangeSeq: (outboundCloseMsg!.header
          .exchangeSeq! as Long).add(1),
      })
    );

    await exchange.exchangeCompleteEvent.awaitEvent();
  });

  test('reordering', async () => {
    const [exchange, _] = testExchange(false);

    const payload1 = new Uint8Array([1, 1, 1]);
    const msg1 = testMessage(
      { exchangeSeq: Long.UZERO, exchangeContinues: true },
      payload1
    );
    const payload2 = new Uint8Array([2, 2, 2]);
    const msg2 = testMessage({ exchangeSeq: Long.UONE }, payload2);

    exchange.processMessage(msg2);
    exchange.processMessage(msg1);

    await expect(exchange.channel[1]).toYield(payload1, payload2);
  });

  test('delivery reordering on open', async () => {
    const [exchange, wireReceiver] = testExchange(true);

    exchange.channel[0].send(new Uint8Array([1]));
    exchange.processMessage(
      errorMessage(EXCHANGE_CLOSED, { exchangeId: exchange.exchangeId })
    );

    // Error should be suppressed
    expect(exchange.channel[1].tryReceiveNow()).toBeNull();
  });
});

describe(UnorderedUnreliable, () => {
  sharedReliabilityTests(UnorderedUnreliable);

  test('old duplicates', async () => {
    const [exchange, _] = testExchange(false, UnorderedUnreliable);

    const dupeMsg = testMessage(
      {
        exchangeSeq: Long.ONE,
        exchangeContinues: true,
      },
      new Uint8Array([1])
    );

    // 300 = DEDUPE_WINDOW_SIZE*3
    for (let i = 1; i < 300; i++) {
      const payload = new Uint8Array([i % 256, i / 256]);
      exchange.processMessage(
        testMessage(
          {
            exchangeSeq: Long.fromNumber(i),
            exchangeContinues: true,
          },
          payload
        )
      );
      exchange.processMessage(dupeMsg);
      expect(await nextMsg(exchange.channel[1])).toEqual(payload);
    }
  });
});
