import { crypto } from '../cryptov1/webcrypto';
import * as keyagreement from '../cryptov1/keyagreement';
import * as keyderivation from '../cryptov1/keyderivation';
import * as signing from '../cryptov1/signing';
import * as testdata from './testdata/cryptov1/firefox.json';
import { stringToBytes } from '../utils/string';

test('generateKeyPair, sign and verify round-trip', async () => {
  const pair = await signing.generateKeyPair();
  const prefix = 'testPrefix';
  const payload = new Uint8Array([1, 2, 3, 4]);
  const signature = await pair.privateKey.signParts(prefix, payload);
  const isValid = await pair.publicKey.verifyParts(signature, prefix, payload);
  expect(isValid).toBe(true);
});

test('verifying testdata signature', async () => {
  const publicKey = new signing.WebCryptoPublicKey(
    await crypto.subtle.importKey(
      'jwk',
      { ...testdata.signingKeyJWK, d: undefined, key_ops: ['verify'] },
      { name: 'ECDSA', namedCurve: 'P-256' },
      true,
      ['verify']
    )
  );

  const signature = Uint8Array.from(atob(testdata.signingSignature), (c) =>
    c.charCodeAt(0)
  );
  const isValid = await publicKey.verifyParts(
    signature,
    'testPrefix',
    stringToBytes('sigPart1'),
    stringToBytes('sigPartTwo')
  );
  expect(isValid).toBe(true);
});

test('keyderivation derive', async () => {
  const output = await keyderivation.deriveBytes(
    'testInfo',
    stringToBytes('testSecret'),
    4
  );
  const expected = Uint8Array.from(
    atob(testdata.keyderivationOutput).split(''),
    (c) => c.codePointAt(0)!
  );
  expect(output).toEqual(expected);
});

test('keyagreement derive', async () => {
  const priv = new keyagreement.WebCryptoPrivateKey(
    await crypto.subtle.importKey(
      'jwk',
      testdata.keyagreementKeyJWK,
      { name: 'ECDH', namedCurve: 'P-256' },
      true,
      ['deriveBits']
    )
  );
  const pub = new keyagreement.WebCryptoPublicKey(
    await crypto.subtle.importKey(
      'jwk',
      { ...testdata.keyagreementKeyJWK, d: undefined },
      { name: 'ECDH', namedCurve: 'P-256' },
      true,
      ['deriveBits']
    )
  );

  const output = await priv.deriveSharedBytes(pub, 16);
  const expected = Uint8Array.from(
    atob(testdata.keyagreementOutput).split(''),
    (c) => c.codePointAt(0)!
  );
  expect(output).toEqual(expected);
});
