import { PeerAuth, PeerAuthInfo } from './peer_auth';
import { deriveBytes } from '../cryptov1/keyderivation';
import { PublicKey } from '../cryptov1/signing';
import { bytesToLowerBase32 } from '../utils/lowerbase32';

const NODE_ID_DERIVE_INFO = 'WLABS-NODE-ID-V1';

export async function nodeIdFromPublicKeyFingerprint(
  publicKey: PublicKey
): Promise<string> {
  const pubBytes = await publicKey.exportRaw();
  const idBytes = await deriveBytes(NODE_ID_DERIVE_INFO, pubBytes, 16);
  return bytesToLowerBase32(idBytes);
}

export class FingerprintIdentityPeerAuth implements PeerAuth {
  constructor(private nodeId: string) {}

  public async checkPeerAuth(peerAuthInfo: PeerAuthInfo) {
    const derivedNodeId = await nodeIdFromPublicKeyFingerprint(
      peerAuthInfo.publicKey
    );
    if (derivedNodeId !== this.nodeId) {
      throw new Error("public key doesn't match node ID");
    }
  }
}
