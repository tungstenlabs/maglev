import { PublicKey } from '../cryptov1/signing';

export interface PeerAuthInfo {
  nodeId: string;
  publicKey: PublicKey;
  credential?: Uint8Array;
}

export interface PeerAuth {
  checkPeerAuth(
    peerAuthInfo: PeerAuthInfo,
    credential?: Uint8Array
  ): Promise<void>;
}

export class TrustedPeerAuth implements PeerAuth {
  public async checkPeerAuth(peerAuthInfo: PeerAuthInfo) {}
}
