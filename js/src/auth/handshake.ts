import * as signing from '../cryptov1/signing';
import * as keyagreement from '../cryptov1/keyagreement';
import * as keyderivation from '../cryptov1/keyderivation';
import { wlabs } from '../gen/proto_gen';
import { ensureBytes } from '../utils/string';
import Long from 'long';
import { toLong } from '../utils/number';
import { Lazy } from '../utils/lazy';

const HELLO_RESPONSE_SIGNATURE_PREFIX = 'WLABS-HELLO-RESPONSE-V1';
const SESSION_ID_DERIVE_INFO = 'WLABS-SESSION-ID-V1';

type Hello = wlabs.maglev.handshake.Hello_WithDefaultValues;
type HelloResponse = wlabs.maglev.handshake.HelloResponse_WithDefaultValues;

export interface HandshakeResults {
  sessionId: Long;
  peerPublicKey: signing.PublicKey;
  peerCredential?: Uint8Array;
}

enum State {
  Init,
  SentHello,
  SentResponse,
  Done,
}

function checkStateTransition(from: State, to: State) {
  const validTransitions = {
    [State.Init]: [State.SentHello, State.SentResponse],
    [State.SentHello]: [State.SentResponse, State.Done],
    [State.SentResponse]: [State.Done],
    [State.Done]: [] as State[],
  };
  if (!validTransitions[from].includes(to)) {
    throw new Error(`invalid transition ${State[from]} -> ${State[to]}`);
  }
}

export class Handshake {
  constructor(
    private readonly keyPair: signing.KeyPair,
    private readonly credential: Uint8Array | string = '',
    private readonly resumeSessionId: Long = Long.UZERO
  ) {}

  /**
   * Returns a Hello message used to initiate the handshake, or null if
   * a Hello has already been processed.
   */
  public async getHello(): Promise<Hello | null> {
    if (this.state !== State.Init) {
      return null;
    }
    this.transition(State.SentHello);
    return {
      version: 1,
      resumeSessionId: this.resumeSessionId,
      ephemeralPublicKey: await this.getEphPubKey(),
    };
  }

  /**
   * Process a received Hello message and return a HelloResponse to continue the
   * handshake.
   */
  public async processHello(hello: Hello): Promise<HelloResponse> {
    this.transition(State.SentResponse);
    if (hello.version != 1) {
      throw new Error(`unsupported Hello version ${hello.version}`);
    }
    const sharedKey = await this.getSharedKey(hello.ephemeralPublicKey);
    const sessionId = await this.getSessionId(
      hello.resumeSessionId ? toLong(hello.resumeSessionId, true) : undefined,
      sharedKey
    );
    return await this.buildHelloResponse(
      sessionId,
      sharedKey,
      await this.getEphPubKey()
    );
  }

  /**
   * Process a received HelloResponse and return handshake results and - if
   * necessary - a HelloResponse to complete the handshake.
   */
  public async processHelloResponse(
    resp: HelloResponse
  ): Promise<[HandshakeResults, HelloResponse?]> {
    const prevState = this.transition(State.Done);
    // Verify signature: "WLABS-HELLO-RESPONSE-V1", <shared ephemeral key>, <token>
    const peerPublicKey = await signing.importRawPublicKey(resp.publicKey);
    const sharedKey = await this.getSharedKey(resp.ephemeralPublicKey);
    if (
      !(await peerPublicKey.verifyParts(
        resp.signature,
        HELLO_RESPONSE_SIGNATURE_PREFIX,
        sharedKey,
        resp.credential
      ))
    ) {
      throw new Error('signature verification failed');
    }

    const sessionId = await this.getSessionId(
      resp.sessionId ? toLong(resp.sessionId, true) : undefined,
      sharedKey
    );

    const results: HandshakeResults = {
      sessionId,
      peerPublicKey,
      peerCredential: resp.credential.length > 0 ? resp.credential : undefined,
    };

    if (prevState == State.SentHello) {
      return [results, await this.buildHelloResponse(sessionId, sharedKey)];
    } else {
      return [results];
    }
  }

  private async buildHelloResponse(
    sessionId: Long,
    sharedKey: Uint8Array,
    ephemeralPublicKey?: Uint8Array
  ): Promise<HelloResponse> {
    // Build signature: "WLABS-HELLO-RESPONSE-V1", <shared ephemeral key>, <token>
    const signature = await this.keyPair.privateKey.signParts(
      HELLO_RESPONSE_SIGNATURE_PREFIX,
      sharedKey,
      this.credential
    );
    return {
      sessionId,
      ephemeralPublicKey,
      credential: ensureBytes(this.credential),
      publicKey: await this.keyPair.publicKey.exportRaw(),
      signature,
    } as HelloResponse;
  }

  private state = State.Init;
  private transition(state: State): State {
    checkStateTransition(this.state, state);
    const prevState = this.state;
    this.state = state;
    return prevState;
  }

  private ephPair = new Lazy(keyagreement.generateKeyPair);

  private async getEphPubKey(): Promise<Uint8Array> {
    const pair = await this.ephPair.get();
    return await pair.publicKey.exportRaw();
  }

  private sharedKey?: Promise<Uint8Array>;
  private async getSharedKey(peerEphPub: Uint8Array): Promise<Uint8Array> {
    return (this.sharedKey ??= (async () => {
      const pair = await this.ephPair.get();
      const peerPub = await keyagreement.importRawPublicKey(peerEphPub);
      return await pair.privateKey.deriveSharedBytes(peerPub, 32);
    })());
  }

  private sessionId?: Promise<Long>;
  private async getSessionId(
    peerSessionId: Long | undefined,
    sharedKey: Uint8Array
  ): Promise<Long> {
    return (this.sessionId ??= (async () => {
      if (
        !this.resumeSessionId.isZero() &&
        this.resumeSessionId.equals(peerSessionId ?? 0)
      ) {
        return this.resumeSessionId;
      } else {
        const sessionIdBytes = await keyderivation.deriveBytes(
          SESSION_ID_DERIVE_INFO,
          sharedKey,
          8
        );
        return Long.fromBytesBE(
          Array.from(new Uint8Array(sessionIdBytes)),
          true
        );
      }
    })());
  }
}

// Public properties only
export type HandshakeIface = { [K in keyof Handshake]: Handshake[K] };
