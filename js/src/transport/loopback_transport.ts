import { AuthContext } from '../auth_context';
import { forward } from '../channels/forward';
import { map } from '../channels/map';
import { SessionContext } from '../session_context';
import { toNumber } from '../utils/number';
import { Transport } from './transport';

export class LoopbackTransport extends Transport {
  constructor(
    nodeId: string,
    authContext: AuthContext,
    sessionContext: SessionContext
  ) {
    super('LoopbackTransport', nodeId, authContext, sessionContext);
    forward(
      map(this.internalIOChannel[1], (msg) => {
        // HACK: this depends on how Sessions avoid exchangeId conflicts...
        let exchangeId = toNumber(msg.header.exchangeId || 0);
        if (exchangeId) {
          exchangeId = exchangeId & 1 ? exchangeId + 1 : exchangeId - 1;
        }
        return {
          header: {
            ...msg.header,
            exchangeId,
          },
          payload: msg.payload,
        };
      }),
      this.internalIOChannel[0]
    );

    void this.beginLifecycle();
  }

  protected async attemptReconnection(): Promise<void> {
    // Nothing to do.
  }
}
