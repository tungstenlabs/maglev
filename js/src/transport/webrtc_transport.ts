import { AuthContext } from '../auth_context';
import { channel } from '../channels/buffered_channel';
import { Channel } from '../channels/channel_interfaces';
import { wlabs } from '../gen/proto_gen';
import { WebRtcConfig } from '../maglev_config';
import { NodeRef } from '../node_ref';
import { SessionContext } from '../session_context';
import { AsyncValue } from '../utils/async_value';
import { none, some } from '../utils/optional';
import { timeout, TimeoutError } from '../utils/timeout';
import { Transport } from './transport';

// Isomorphic RTCPeerConnection (browser or wrtc for node).
const IsoRTCPeerConnection: typeof RTCPeerConnection =
  typeof window != 'undefined' && window.RTCPeerConnection
    ? window.RTCPeerConnection
    : // eslint-disable-next-line @typescript-eslint/no-var-requires
      require('wrtc').RTCPeerConnection;

type WebRtcSignal = wlabs.maglev.webrtc.WebRtcSignal;

export class WebRtcTransport extends Transport {
  private rtcPeerConnection: RTCPeerConnection;
  private rtcDataChannel: RTCDataChannel;
  private rtcConnected = new AsyncValue<boolean>();
  private rtcError = new AsyncValue<RTCErrorEvent>();

  constructor(
    destNodeId: string,
    authContext: AuthContext,
    sessionContext: SessionContext,
    wrtcConfig: WebRtcConfig,
    nodeRef: NodeRef
  ) {
    super('WebRtcTransport', destNodeId, authContext, sessionContext);

    // Both sides can immediately create the RTCPeerConnection object to start
    // gathering ICE candidates. The "perfect negotiation" flow is symmetric,
    // the only thing different is where the signalling stream comes from.
    this.rtcPeerConnection = new IsoRTCPeerConnection({
      iceServers: wrtcConfig.iceServers,
    });

    // Now we can create the RTCDataChannel, which doesn't require that the
    // RtcPeerConnection be connected yet.
    this.rtcDataChannel = this.rtcPeerConnection.createDataChannel(
      '__maglev__',
      {
        ordered: false,
        negotiated: true,
        id: 0,
      }
    );
    this.rtcDataChannel.binaryType = 'arraybuffer';

    // If we are the polite peer, then we register an RPC handler and wait for
    // the other node to open the signalling stream. We don't bother to
    // unregister as it's assumed this Transport will last as long as the
    // NodeRef object.
    const polite = authContext.nodeId < this.destNodeId;
    if (polite) {
      wlabs.maglev.webrtc.WebRtcService.registerHandler(nodeRef, {
        signal: (req, _ctx) => {
          const [sender, receiver] = channel<WebRtcSignal>();
          // From here we send the stream over to the perfect negotiation flow.
          void this.rtcPerfectNegotiationFlow(polite, [sender, req]);
          return receiver;
        },
      });
      this.log.trace('registered WebRtcService');
    } else {
      // The impolite peer gets the signalling channel by calling the RPC on the
      // other end. There shouldn't be a race of the other node not having
      // registered the handler yet, as we register it in this constructor.
      const client = wlabs.maglev.webrtc.WebRtcService.createClient(nodeRef);
      const [sender, receiver] = channel<WebRtcSignal>();
      const res = client.signal(receiver);
      void this.rtcPerfectNegotiationFlow(polite, [sender, res]);
    }

    // Forward traffic through the data channel
    const [sender, receiver] = this.internalIOChannel;
    const rtcDataChannel = this.rtcDataChannel;

    void (async () => {
      for await (const res of receiver) {
        if (res.err) {
          return this.log.error(res.err);
        }

        await this.rtcConnected.get();
        try {
          rtcDataChannel.send(this.encodePrefixedMaglevMsg(res.msg));
        } catch (e) {
          this.transportDisconnected(e);
        }
      }
    })();

    this.rtcDataChannel.onmessage = (ev) => {
      sender.send(this.decodePrefixedMaglevMsg(new Uint8Array(ev.data)));
    };

    // Register lifecycle handlers.
    this.rtcDataChannel.onopen = () => {
      this.log.trace('WebRTC open event');
      this.rtcConnected.set(some(true));
      this.rtcError.set(none());
    };

    this.rtcDataChannel.onerror = (e) => {
      this.log.trace('WebRTC error event', e);
      this.rtcConnected.set(none());
      this.rtcError.set(some(e));
      this.transportDisconnected();
    };

    this.rtcPeerConnection.onicecandidateerror = (ev) => {
      this.log.trace('ICE candiate error:', ev);
    };

    this.rtcPeerConnection.oniceconnectionstatechange = (ev) => {
      if (
        this.rtcPeerConnection.connectionState === 'disconnected' &&
        this.rtcConnected.value.valueOrNull() === true
      ) {
        this.log.trace('WebRTC close event');
        this.rtcConnected.set(none());
        this.rtcError.set(none());
        this.transportDisconnected();
      }
    };

    void this.beginLifecycle();
  }

  public dispose(): void {
    super.dispose();
    try {
      this.rtcDataChannel.close();
      this.rtcPeerConnection.close();
      this.internalIOChannel[0].close();
      this.rtcConnected.set(none());
    } catch (e) {
      console.warn('[WebRtcTransport::dispose]: close error:', e);
    }
  }

  protected async attemptReconnection() {
    // Race between connecting, error, or timeout
    try {
      const connectedOrError = await Promise.any<
        boolean | RTCErrorEvent | void
      >([
        this.rtcConnected.get(),
        this.rtcError.get(),
        timeout(
          30_000,
          new TimeoutError('WebRTC datachannel connection timeout')
        ),
      ]);

      if (connectedOrError === true) {
        return;
      }

      throw connectedOrError;
    } catch (e) {
      // TS doesn't seem to have definitions for restartIce -rolls eyes-
      (this.rtcPeerConnection as any).restartIce();
      throw e;
    }
  }

  private async rtcPerfectNegotiationFlow(
    polite: boolean,
    signalingChannel: Channel<WebRtcSignal>
  ) {
    const [sender, receiver] = signalingChannel;
    const rtcPeerConnection = this.rtcPeerConnection;

    let makingOffer = false;
    let ignoreOffer = false;

    rtcPeerConnection.onicecandidate = ({ candidate }) => {
      // Test ye relay by un-commenting this disgusting hack.
      // if (JSON.stringify(candidate).indexOf('typ relay') < 0) {
      //   return;
      // }
      if (candidate) {
        sender.send({ candidateJson: JSON.stringify(candidate) });
      }
    };

    rtcPeerConnection.onnegotiationneeded = async () => {
      this.log.trace('negotiation needed');
      try {
        makingOffer = true;
        const offer = await rtcPeerConnection.createOffer();
        if (rtcPeerConnection.signalingState != 'stable') {
          return;
        }
        await rtcPeerConnection.setLocalDescription(offer);
        sender.send({
          descriptionJson: JSON.stringify(rtcPeerConnection.localDescription),
        });
      } catch (e) {
        console.warn('onnegotiationneeded exception:', e);
      } finally {
        makingOffer = false;
      }
    };

    // Finally handle incoming signals
    for await (const res of receiver) {
      if (res.err) {
        return this.log.error(res.err);
      }

      const signal = res.msg;

      if (signal.descriptionJson) {
        const description = JSON.parse(signal.descriptionJson);
        const offerCollision =
          description.type == 'offer' &&
          (makingOffer || rtcPeerConnection.signalingState != 'stable');

        ignoreOffer = !polite && offerCollision;
        if (ignoreOffer) {
          return;
        }
        if (offerCollision) {
          await Promise.all([
            rtcPeerConnection.setLocalDescription({
              type: 'rollback',
            }),
            rtcPeerConnection.setRemoteDescription(description),
          ]);
        } else {
          await rtcPeerConnection.setRemoteDescription(description);
        }
        if (description.type == 'offer') {
          await rtcPeerConnection.setLocalDescription(
            await rtcPeerConnection.createAnswer()
          );
          sender.send({
            descriptionJson: JSON.stringify(rtcPeerConnection.localDescription),
          });
        }
      } else if (signal.candidateJson) {
        const candidate = JSON.parse(signal.candidateJson);
        try {
          await rtcPeerConnection.addIceCandidate(candidate);
        } catch (e) {
          if (!ignoreOffer) {
            console.warn('Handling incoming ICE candidate exception:', e);
          }
        }
      }
    }
  }
}
