import Long from 'long';
import protobuf from 'protobufjs';
import { Handshake, HandshakeIface, HandshakeResults } from '../auth/handshake';
import { AuthContext } from '../auth_context';
import { biChannel, channel } from '../channels/buffered_channel';
import {
  Channel,
  ChannelReceiver,
  ChannelSender,
} from '../channels/channel_interfaces';
import { wlabs } from '../gen/proto_gen';
import { protoRoot } from '../rpc/proto_registry';
import { SessionContext } from '../session_context';
import { Log, maglevLog } from '../utils/log';
import { isZero } from '../utils/number';
import { ObservableValue } from '../utils/observable';
import { sleep } from '../utils/time';
import { TimeoutError } from '../utils/timeout';
import { StatusError } from './errors';

type MsgHeader = wlabs.maglev.transport.MsgHeader;

/**
 * Data cannot be sent over a Transport until the handshake is complete, doing
 * so will result in a throw. It's the connections job to buffer data until a
 * transport is ready to send it.
 */
export abstract class Transport {
  /**
   * How long to wait between each failed attempts at calling
   * `attemptReconnection` (doesn't have to do with the specific transport).
   */
  protected static RETRY_TIMER_MS = 5_000;

  /**
   * The timeout on attempting a handshake over a newly opened connection.
   */
  protected static HANDSHAKE_TIMEOUT_MS = 10_000;

  /**
   * Used by a `Connection` to send and receive data across this Transport.
   */
  public ioChannel: Channel<TransportMessage>;

  /**
   * True if the Transport is fully connected and ready to send bytes.
   */
  public readonly connected = new ObservableValue(false);

  /**
   * The most recent error.
   */
  public error?: Error;

  /**
   * The most recent handshake results.
   *
   * Observable.
   */
  public readonly handshakeResults = new ObservableValue<HandshakeResults | null>(
    null
  );

  protected msgHeaderType = protoRoot.lookupType(
    'wlabs.maglev.transport.MsgHeader'
  );
  protected helloType = protoRoot.lookupType('wlabs.maglev.handshake.Hello');
  protected helloResponseType = protoRoot.lookupType(
    'wlabs.maglev.handshake.HelloResponse'
  );
  protected log: Log;
  protected disposed = false;

  /**
   * The I/O channel a Transport subclass uses to communicate with the transport
   * owner.
   */
  protected internalIOChannel: Channel<TransportMessage>;

  private static HANDSHAKE_TIMEOUT = new TimeoutError('Handshake timed out');

  /**
   * Messages after the handshake is complete are written to this. They end up
   * coming out of the ioChannel Receiver.
   */
  private connectedMessageSender: ChannelSender<TransportMessage>;

  /**
   * The raw receiver from the transport.
   */
  private rawReceiver: ChannelReceiver<TransportMessage>;

  protected constructor(
    public name: string,
    public destNodeId: string,
    protected authContext: AuthContext,
    protected sessionContext: SessionContext
  ) {
    this.log = maglevLog.child(`${this.name}:${this.destNodeId}`);

    // Channel wiring is a little confusing here. It maps as...
    //  -- Subclass handles:
    //     - internalIoChannel::Receiver -> send over wire
    //     - receiver from wire -> internalIoChannel::Sender
    //  -- We handle:
    //  (Connection) -> ioChannel::Sender -> internalIoChannel::Receiver
    //  internalIoChannel::Sender -> rawReceiver
    //  rawReceiver -> postHandshakeSender (done in `handshake`)
    //  postHandshakeSender -> ioChannel::Receiver -> (Connection)

    // It's setup this convoluted way to allow for easy use in subclasses, and
    // because `handshake` does filtering of messages.
    const [
      [ioChannelSender, rawReceiver],
      internalIOChannel,
    ] = biChannel<TransportMessage>();
    this.rawReceiver = rawReceiver;
    this.internalIOChannel = internalIOChannel;
    const [
      connectedMessageSender,
      ioChannelReceiver,
    ] = channel<TransportMessage>();
    this.connectedMessageSender = connectedMessageSender;
    this.ioChannel = [ioChannelSender, ioChannelReceiver];
  }

  /**
   * Transport specific dispose method, called by a `Connection`.
   */
  public dispose(): void {
    if (this.disposed) {
      return;
    }

    // Note: We don't dispatch an event because `dispose` is always called
    // intentionally.
    this.disposed = true;
    this.ioChannel[0].close();
    this.internalIOChannel[0].close();
    this.connectedMessageSender.close();
  }

  /**
   * The primary method implemented by subclasses to 'connect' the transport.
   * This can throw Error instances for the failure case, they will be caught.
   */
  protected abstract attemptReconnection(): Promise<void>;

  /**
   * The main lifecycle loop of this Transport. This must be called by derived
   * classes once they are ready to have `attemptReconnection` called.
   */
  protected async beginLifecycle() {
    void this.inboundLoop();

    while (!this.disposed) {
      // Attempt connection and start handshake
      try {
        // Transport-specific connection logic
        await this.attemptReconnection();

        await this.startHandshake();
      } catch (e) {
        this.log.debug('transport connect failed:', e);
        await sleep(Transport.RETRY_TIMER_MS);
        continue;
      }

      // Wait for successful connection, with timeout
      const success = await this.connected.whenEqual(
        true,
        Transport.HANDSHAKE_TIMEOUT_MS
      );
      if (!success) {
        this.handshake = undefined;
        this.log.debug('transport connect timed out');
        continue;
      }

      // We are connected and healthy, wait for that to change to reconnect
      console.assert(this.connected);
      await this.connected.whenEqual(false);
    }
  }

  private async inboundLoop() {
    for await (const { msg, err } of this.rawReceiver) {
      if (err || !msg) {
        this.connectedMessageSender.fail(err ?? new Error('no err or msg!'));
        return;
      }

      // If a message is from (vs being relayed by) the transport peer, and it
      // is a transport control message (exchange zero), it should be handled by
      // this transport.
      const isFromTransportPeer =
        !msg.header.srcNodeId || msg.header.srcNodeId === this.destNodeId;
      if (isFromTransportPeer && isTransportControlMsg(msg)) {
        await this.handleControlMsg(msg);
      } else if (this.connected.get()) {
        this.connectedMessageSender.send(msg);
      } else {
        this.log.warn('Got message on disconnected transport:', msg);
      }
    }
  }

  // Handle control (e.g. handshake) messages
  protected async handleControlMsg(msg: TransportMessage) {
    if (
      msg.header.destNodeId &&
      msg.header.destNodeId !== this.authContext.nodeId
    ) {
      console.error('Got transport control message for another node:', msg);
      return;
    }

    const maybeError = StatusError.fromMessage(msg);
    if (maybeError) {
      this.transportDisconnected(maybeError);
      return;
    }

    const type = msg.header.exchangeType;
    if (type === 'Hello' || type === 'HelloResponse') {
      try {
        await this.handleHandshakeMsg(msg);
      } catch (e) {
        this.log.warn('Error processing handshake message:', e);
      }
    } else {
      this.log.warn('Unknown transport control message type:', type);
    }
  }

  // Send control (e.g. handshake) messages
  private sendControlMsg(exchangeType: string, payload: Uint8Array) {
    this.log.trace('sending', exchangeType);
    this.ioChannel[0].send({ header: { exchangeType }, payload });
  }

  // Handshake

  protected handshake?: HandshakeIface;

  protected newHandshake(): HandshakeIface {
    return new Handshake(
      this.authContext.keyPair,
      this.authContext.credential,
      this.sessionContext.sessionId.value.valueOrNull() || undefined
    );
  }

  private async startHandshake() {
    this.handshake ??= this.newHandshake();
    const hello = await this.handshake.getHello();
    if (hello) {
      const payload = wlabs.maglev.handshake.Hello.encode(hello);
      this.sendControlMsg('Hello', payload);
    }
  }

  private async handleHandshakeMsg(msg: TransportMessage) {
    this.handshake ??= this.newHandshake();
    const type = msg.header.exchangeType;
    let reply: wlabs.maglev.handshake.HelloResponse | undefined;
    if (type === 'Hello') {
      const hello = wlabs.maglev.handshake.Hello.decode(msg.payload!);
      reply = await this.handshake.processHello(hello);
    } else if (type === 'HelloResponse') {
      const helloResp = wlabs.maglev.handshake.HelloResponse.decode(
        msg.payload!
      );
      let results: HandshakeResults;
      [results, reply] = await this.handshake.processHelloResponse(helloResp);

      // Connection successful
      this.log.trace('connection successful:', results.sessionId.toString());
      this.handshakeResults.set(results);
      this.handshake = undefined;
      this.connected.set(true);
    } else {
      // Unreachable (?!)
      throw `invalid handshake type ${type}`;
    }
    if (reply) {
      this.sendControlMsg(
        'HelloResponse',
        wlabs.maglev.handshake.HelloResponse.encode(reply)
      );
    }
  }

  /**
   * Should be called by subclasses on disconnect.
   */
  protected transportDisconnected(err?: any) {
    this.log.debug('transport disconnected:', err);
    this.error = err;
    this.connected.set(false);
  }

  /**
   * Decodes a standard MsgHeader prefixed data blob, into a TransportMessage.
   */
  protected decodePrefixedMaglevMsg(data: Uint8Array): TransportMessage {
    const reader = new protobuf.Reader(data);
    const header = this.msgHeaderType.toObject(
      this.msgHeaderType.decodeDelimited(reader),
      { defaults: true }
    ) as MsgHeader;
    const msg: TransportMessage = { header };
    if (!header.noPayload) {
      msg.payload = data.subarray(reader.pos, data.length);
    }
    this.traceMessage(msg, false);
    return msg;
  }

  /**
   * Encodes a standard TransportMessage into a byte array.
   */
  protected encodePrefixedMaglevMsg(msg: TransportMessage): Uint8Array {
    this.traceMessage(msg, true);
    let data = this.msgHeaderType.encodeDelimited(msg.header).finish();
    if (!msg.header.noPayload && msg.payload) {
      const dataWithPayload = new Uint8Array(data.length + msg.payload.length);
      dataWithPayload.set(data, 0);
      dataWithPayload.set(msg.payload, data.length);
      data = dataWithPayload;
    }
    return data;
  }

  private traceMessage(msg: TransportMessage, sending: boolean) {
    const dir = sending ? 'S' : 'R';
    const typeOf = this.name;
    const other = sending
      ? msg.header.destNodeId || this.destNodeId
      : msg.header.srcNodeId || this.destNodeId;
    const ex = (msg.header.exchangeId || Long.fromString('0', true)).toString();
    const sq = msg.header.exchangeSeq?.toString() || '0';
    const ack = msg.header.expectingExchangeSeq
      ? ` ACK ${msg.header.expectingExchangeSeq.toString()}`
      : '';
    const cnt = msg.header.exchangeContinues ? 'CNT' : 'END';
    const type = msg.header.exchangeType ? ` T/${msg.header.exchangeType}` : '';
    const len = msg.payload?.length || 0;
    const hasError = msg.header.error && msg.header.error.code;
    const err = hasError
      ? ` E/${msg.header.error!.code}/: ${msg.header.error?.message}`
      : '';
    const full = `-> ${typeOf} ${dir} ${other} ${ex}|${sq}${ack} ${len}b ${cnt}${type}${err}`;
    if (msg.header.reliabilityMsg) {
      maglevLog.trace(full, '(empty ACK)');
    } else if (hasError) {
      maglevLog.warn(full);
    } else {
      maglevLog.trace(full);
    }
  }
}

export interface TransportMessage {
  header: Partial<MsgHeader>;
  payload?: Uint8Array;
}

export function isTransportControlMsg(msg: TransportMessage): boolean {
  return msg.header.exchangeId === undefined || isZero(msg.header.exchangeId);
}
