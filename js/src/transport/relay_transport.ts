import { AuthContext } from '../auth_context';
import { forward } from '../channels/forward';
import { map } from '../channels/map';
import { SessionContext } from '../session_context';
import { Transport, TransportMessage } from './transport';

export class RelayTransport extends Transport {
  constructor(
    destNodeId: string,
    authContext: AuthContext,
    sessionContext: SessionContext,
    private relayingTransport: Transport
  ) {
    super('RelayTransport', destNodeId, authContext, sessionContext);
    // Only forward outbound traffic; inbound will be handled by the Router.
    forward(
      map(this.internalIOChannel[1], (msg) => {
        msg.header.destNodeId ??= this.destNodeId;
        return msg;
      }),
      relayingTransport.ioChannel[0]
    );
    void this.beginLifecycle();
  }

  public async handleControlMsg(msg: TransportMessage) {
    return super.handleControlMsg(msg);
  }

  protected async attemptReconnection(): Promise<void> {
    await this.relayingTransport.connected.whenEqual(true);
  }
}
