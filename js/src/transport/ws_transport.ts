import { AuthContext } from '../auth_context';
import { SessionContext } from '../session_context';
import { AsyncValue } from '../utils/async_value';
import { Log, LogLevel } from '../utils/log';
import { none, some } from '../utils/optional';
import { Transport } from './transport';

// Isomorphic Websocket (browser or websocket for node)
const IsoWebSocket: typeof WebSocket =
  typeof window != 'undefined' && window.WebSocket
    ? window.WebSocket
    : // eslint-disable-next-line @typescript-eslint/no-var-requires
      require('websocket').w3cwebsocket;

export class WSTransport extends Transport {
  private websocket = new AsyncValue<WebSocket>();

  constructor(
    destNodeId: string,
    authContext: AuthContext,
    sessionContext: SessionContext,
    private url: string
  ) {
    super('WSTransport', destNodeId, authContext, sessionContext);
    void this.outboundLoop();
    void this.beginLifecycle();
  }

  private async outboundLoop() {
    try {
      for await (const res of this.internalIOChannel[1]) {
        if (res.err) {
          this.log.error(res.err);
          this.cleanup();
          return;
        }

        const websocket = await this.websocket.get();
        this.log.assert(websocket.readyState === IsoWebSocket.OPEN);
        websocket.send(this.encodePrefixedMaglevMsg(res.msg));
      }
    } catch (err) {
      this.log.error('outboundLoop error:', err);
      this.cleanup();
    }
  }

  public dispose(): void {
    super.dispose();
    this.cleanup();
  }

  protected async attemptReconnection() {
    this.log.trace('Connecting to ', this.url);

    const websocket = new IsoWebSocket(this.url);
    websocket.binaryType = 'arraybuffer';

    if (Log.level >= LogLevel.DEBUG) {
      // Reach into W3CWebSocket guts to get error messages in Node.
      try {
        const client = (websocket as any)._client;
        if (client) {
          client.on('connectFailed', (err: any) => {
            this.log.debug('WS connection error:', err);
          });
        }
      } catch (e) {
        this.log.error(e);
      }
    }

    websocket.onmessage = (ev) =>
      this.internalIOChannel[0].send(
        this.decodePrefixedMaglevMsg(new Uint8Array(ev.data))
      );

    // Wait for the Websocket to open before sending messages across it.
    await new Promise((res, rej) => {
      websocket.onopen = res;
      websocket.onerror = rej;
    });

    websocket.onerror = (_ev) => this.cleanup();
    websocket.onclose = (_ev) => this.cleanup();

    this.websocket.set(some(websocket));
  }

  private cleanup() {
    if (this.websocket.value.type === 'none') {
      return;
    }
    try {
      const websocket = this.websocket.value.unwrap();
      websocket.onerror = () => {};
      websocket.onclose = () => {};
      websocket.close();
    } finally {
      this.websocket.set(none());
      this.transportDisconnected();
    }
  }
}
