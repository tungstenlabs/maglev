import Long from 'long';
import { biChannel } from './channels/buffered_channel';
import { Channel, ChannelSender } from './channels/channel_interfaces';
import {
  Code,
  errorMessage,
  isErrorMessage,
  StatusError,
} from './transport/errors';
import { TransportMessage } from './transport/transport';
import { BufferedEvent } from './utils/buffered_event';
import { isZero, toNumber } from './utils/number';

export type ReliabilityType =
  | typeof OrderedReliable
  | typeof UnorderedUnreliable;

type ExchangeDirection = 'recv' | 'send';

export class Exchange {
  /**
   * The channel the client using this class sends and receives payloads over.
   */
  public readonly channel: Channel<Uint8Array>;

  /**
   * Fires when the exchange has finished processing all inbound and outbound
   * messages (including e.g. retransmissions)
   */
  public readonly exchangeCompleteEvent = new BufferedEvent<Error | null>();

  private readonly clientChannel: Channel<Uint8Array>;
  private readonly reliabilityState: ReliabilityStrategy;

  private nextSeq = 0;
  private isClosed = false;
  private error?: Error;

  constructor(
    public readonly exchangeId: Long,
    public readonly exchangeType: string,

    // True iff this node is initiating the exchange.
    public readonly initiating: boolean,

    // Reliability type to use. Must match with peer.
    public readonly reliabilityType: ReliabilityType,

    /**
     * Channel used to read/write data cross the wire. A `Connection` provides
     * this channel, and will fail it if the connection times out.
     */
    private readonly wireSender: ChannelSender<TransportMessage>
  ) {
    [this.channel, this.clientChannel] = biChannel();
    this.reliabilityState = new reliabilityType();

    void this.outboundLoop();
  }

  public processMessage(msg: TransportMessage) {
    this.reliabilityState.processInbound(msg);
    void this.dispatchExchangeActions();
  }

  public close(err?: Error) {
    if (this.isClosed) {
      return;
    }
    this.isClosed = true;
    this.error = err;

    this.reliabilityState.complete();

    // Close/fail channels
    if (err) {
      this.clientChannel[0].fail(err);
      this.clientChannel[1].fail(err);
    } else {
      this.clientChannel[0].close();
    }

    void this.dispatchExchangeActions();
  }

  private async outboundLoop() {
    try {
      const [_, nodeRecv] = this.clientChannel;

      // Send either initial payload or empty payload if channel is `open`ed
      await nodeRecv.opened();
      const maybeInitialMsg = nodeRecv.tryReceiveNow();
      this.processOutboundPayload(
        maybeInitialMsg?.msg ?? null,
        !nodeRecv.isDone
      );

      // Send the middle payloads
      let doneSending = nodeRecv.isDone;
      for await (const res of nodeRecv) {
        if (res.err) {
          return this.processOutboundError(res.err);
        }

        this.processOutboundPayload(res.msg, !nodeRecv.isDone);
        doneSending ||= nodeRecv.isDone;
      }

      // Send a final empty message if needed
      if (!doneSending) {
        this.processOutboundPayload(null, false);
      }
    } catch (err) {
      this.processOutboundError(err);
    }
  }

  private exchangeActionsTimerId?: ReturnType<typeof setTimeout>;
  private dispatchExchangeActionsRunning = false;

  private async dispatchExchangeActions() {
    if (this.exchangeActionsTimerId) {
      clearTimeout(this.exchangeActionsTimerId);
    }

    // Only run one dispatch at a time.
    if (this.dispatchExchangeActionsRunning) {
      return;
    }
    this.dispatchExchangeActionsRunning = true;

    try {
      while (true) {
        // Dispatch action
        const action = this.reliabilityState.nextAction();
        if (action.type === 'recv') {
          // Inbound message; dispatch to user code
          const msg = action.msg;

          // Handle errors
          const err = StatusError.fromMessage(msg);
          if (err) {
            this.close(err);
            return;
          }

          // Dispatch payload to user code (and maybe close)
          const [nodeSend] = this.clientChannel;
          const hasPayload = !msg.header.noPayload;
          const isFinal = !msg.header.exchangeContinues;
          if (hasPayload && isFinal) {
            nodeSend.sendAndClose(msg.payload!);
          } else if (hasPayload) {
            nodeSend.send(msg.payload!);
          } else if (isFinal) {
            nodeSend.close();
          }

          if (isFinal) {
            this.halfClose('recv');
          }
        } else if (action.type === 'send') {
          // Outbound message; deliver to wire (session)
          const msg = action.msg;
          msg.header.exchangeId = this.exchangeId;
          if (this.initiating && isZero(msg.header.exchangeSeq!)) {
            msg.header.exchangeType = this.exchangeType;
          }

          try {
            this.wireSender.send(msg);
          } catch (err) {
            this.close(err);
            return;
          }

          const err = StatusError.fromMessage(msg);
          if (err) {
            this.close(err);
            return;
          }

          if (!msg.header.exchangeContinues) {
            this.halfClose('send');
          }
        } else if (action.type === 'wait') {
          // Wait for next action
          if (!action.time) {
            // No scheduled actions; wait for next inbound/outbound message
            return;
          }
          // Wait for scheduled action (unless its already time for it)
          const delay = action.time - Date.now();
          if (delay > 0) {
            this.exchangeActionsTimerId = setTimeout(
              () => this.dispatchExchangeActions(),
              delay
            );
            return;
          }
        } else if (action.type === 'done') {
          this.exchangeCompleteEvent.dispatchEvent(this.error ?? null);
          return;
        } else {
          const _: never = action;
        }
      }
    } finally {
      this.dispatchExchangeActionsRunning = false;
    }
  }

  private processOutboundPayload(
    payload: Uint8Array | null,
    exchangeContinues: boolean
  ) {
    this.processOutboundMsg({
      header: {
        exchangeContinues,
        noPayload: !payload,
      },
      payload: payload || undefined,
    });
  }

  private processOutboundError(err: any) {
    if (!(err instanceof Error)) {
      err = new Error(`<${err}>`);
    }
    this.processOutboundMsg(errorMessage(err));
  }

  private processOutboundMsg(msg: TransportMessage) {
    msg.header.exchangeSeq = Long.fromNumber(this.nextSeq, true);
    this.nextSeq++;
    this.reliabilityState.processOutbound(msg);
    void this.dispatchExchangeActions();
  }

  private halfClosed?: ExchangeDirection;

  private halfClose(dir: ExchangeDirection) {
    if (this.halfClosed && this.halfClosed !== dir) {
      this.close();
    } else {
      this.halfClosed = dir;
    }
  }
}

type ExchangeAction = Recv | Send | Wait | Done;
type ExchangeActionType = ExchangeAction['type'];

interface Recv {
  type: 'recv';
  msg: TransportMessage;
}

interface Send {
  type: 'send';
  msg: TransportMessage;
}

interface Wait {
  type: 'wait';
  // If set, the timestamp of the next scheduled send.
  time?: number;
}

interface Done {
  type: 'done';
}

interface ReliabilityStrategy {
  /** Update engine state with an inbound message */
  processInbound(msg: TransportMessage, now?: number): void;
  /** Update engine state with an outbound message */
  processOutbound(msg: TransportMessage, now?: number): void;
  /**
   * Update engine state to "completed"; no new payloads will be queued, but
   * `nextAction` should be processed until type "done" is returned.
   */
  complete(): void;
  /** Return the next action to process */
  nextAction(now?: number): ExchangeAction;
}

export class UnorderedUnreliable implements ReliabilityStrategy {
  private static DEDUPE_WINDOW_SIZE = 100;

  private readonly actions: ExchangeAction[] = [];
  private done = false;
  public processInbound(msg: TransportMessage) {
    if (this.done || this.isDuplicate(seqOf(msg))) {
      return;
    }
    if (msg.header.reliabilityMsg) {
      console.warn(
        'Got reliabilityMsg on unreliable exchange',
        msg.header.exchangeId?.toString()
      );
      return;
    }
    if (isErrorMessage(msg)) {
      this.actions.length = 0;
    }
    this.actions.push({ type: 'recv', msg });
  }
  public processOutbound(msg: TransportMessage) {
    if (this.done) {
      return;
    }
    if (isErrorMessage(msg)) {
      this.actions.length = 0;
    }
    this.actions.push({ type: 'send', msg });
  }
  public complete() {
    this.done = true;
  }
  public nextAction(): ExchangeAction {
    if (this.done) {
      return { type: 'done' };
    }
    return this.actions.shift() || { type: 'wait' };
  }

  // Inbound deduplication
  private minInboundSeq = 0;
  private inboundSeqsSeen = new Set<number>();
  private isDuplicate(seq: number): boolean {
    // If outside of window or already seen, its a dupe
    if (seq < this.minInboundSeq || this.inboundSeqsSeen.has(seq)) {
      return true;
    }
    // Update dedupe window
    const newMinInboundSeq = seq - UnorderedUnreliable.DEDUPE_WINDOW_SIZE;
    if (newMinInboundSeq > this.minInboundSeq) {
      this.minInboundSeq = newMinInboundSeq;
    }
    // Truncate seqs seen set if its getting too big
    if (
      this.inboundSeqsSeen.size >
      UnorderedUnreliable.DEDUPE_WINDOW_SIZE * 2
    ) {
      for (const seenSeq of this.inboundSeqsSeen) {
        if (seenSeq < this.minInboundSeq) {
          this.inboundSeqsSeen.delete(seenSeq);
        }
      }
    }
    // Update seqs seen set
    this.inboundSeqsSeen.add(seq);
    return false;
  }
}

export class OrderedReliable implements ReliabilityStrategy {
  // TODO: optimize these values
  private static INITIAL_RETRANSMIT_TIMEOUT = 1000;
  private static MAXIMUM_RETRANSMIT_TIMEOUT = 10000;
  private static EXPECTING_SEND_TIMEOUT = 50;

  private readonly reorderingBuffer = new MessageSequence();
  private readonly transmitBuffer = new MessageSequence<ReliableTransportMessage>();

  private nextInboundSeq = 0;
  private completing = false;

  private expectingSendDeadline?: number;

  public processInbound(msg: TransportMessage): void {
    // Remove acknowledged messages from retransmit buffer
    const peerExpectingSeq = toNumber(msg.header.expectingExchangeSeq ?? 0);
    if (peerExpectingSeq) {
      this.transmitBuffer.deleteSeqsLessThan(peerExpectingSeq);
    }
    // Done with "empty ACKs"
    if (msg.header.reliabilityMsg) {
      return;
    }

    if (this.completing) {
      if (isErrorMessage(msg)) {
        // Error means peer wouldn't process retransmits
        this.transmitBuffer.clear();
      }
      return;
    }

    // Drop duplicate messages
    if (seqOf(msg) < this.nextInboundSeq) {
      return;
    }

    if (isErrorMessage(msg)) {
      // Suppress "Failed Precondition" (e.g. "exchange closed") errors iff we
      // haven't received an opening message or ACK. This handles seq=0 being
      // delivered after a later message.
      if (
        msg.header.error.code === Code.FailedPrecondition &&
        this.nextInboundSeq === 0 &&
        peerExpectingSeq === 0
      ) {
        return;
      }
      // Error becomes last and only message left to process
      this.nextInboundSeq = seqOf(msg);
      this.reorderingBuffer.clear();
    }

    // Queue message for delivery
    this.reorderingBuffer.add(msg);
    // TODO: enforce maximum reordering buffer size
  }

  public processOutbound(
    msg: TransportMessage,
    now: number = Date.now()
  ): void {
    if (this.completing) {
      return;
    }

    if (isErrorMessage(msg)) {
      // Replace outbound queue with error
      this.transmitBuffer.clear();
    }
    this.transmitBuffer.add({
      ...msg,
      nextTransmitTime: now,
      retransmitTimeout: OrderedReliable.INITIAL_RETRANSMIT_TIMEOUT,
    });
  }

  public complete() {
    if (this.completing) {
      return;
    }
    this.completing = true;

    // Don't need to dispatch any more inbound after complete
    this.reorderingBuffer.clear();
  }

  public nextAction(now: number = Date.now()): ExchangeAction {
    return this.nextScheduledAction(now, true);
  }

  private nextScheduledAction(now: number, advance: boolean): ExchangeAction {
    // Prioritize any ordered inbound message
    const inboundMsg = this.reorderingBuffer.first();
    if (inboundMsg && seqOf(inboundMsg) === this.nextInboundSeq) {
      if (advance) {
        this.reorderingBuffer.shift();
        this.nextInboundSeq++;
        this.expectingSendDeadline ??=
          now + OrderedReliable.EXPECTING_SEND_TIMEOUT;
      }
      return { type: 'recv', msg: inboundMsg };
    }

    // Find the next scheduled outbound message:
    let outboundMsg: TransportMessage | undefined = undefined;
    let sendTime = Infinity;

    // * empty ACK
    if (this.expectingSendDeadline) {
      outboundMsg = {
        header: { reliabilityMsg: true },
      };
      sendTime = this.expectingSendDeadline;
    }

    // * (re)transmission
    let reliableMsg: ReliableTransportMessage | undefined = undefined;
    for (const txMsg of this.transmitBuffer) {
      if (txMsg.nextTransmitTime < sendTime) {
        outboundMsg = reliableMsg = txMsg;
        sendTime = txMsg.nextTransmitTime;
      }
    }

    // Nothing is scheduled and we're closing: Done
    if (!outboundMsg && this.completing) {
      return { type: 'done' };
    }

    // Nothing to send immediately: Wait
    if (!outboundMsg || sendTime > now) {
      const time = sendTime < Infinity ? sendTime : undefined;
      return { type: 'wait', time };
    }

    if (advance) {
      // Update ACK, even for retransmits
      outboundMsg.header.expectingExchangeSeq = Long.fromNumber(
        this.nextInboundSeq,
        true
      );
      this.expectingSendDeadline = undefined;

      // Schedule next retransmit
      if (reliableMsg) {
        reliableMsg.nextTransmitTime = now + reliableMsg.retransmitTimeout;
        reliableMsg.retransmitTimeout = Math.max(
          reliableMsg.retransmitTimeout * 2,
          OrderedReliable.MAXIMUM_RETRANSMIT_TIMEOUT
        );
      }
    }

    return { type: 'send', msg: outboundMsg };
  }
}

interface ReliableTransportMessage extends TransportMessage {
  nextTransmitTime: number;
  retransmitTimeout: number;
}

class MessageSequence<Msg extends TransportMessage = TransportMessage> {
  // Invariants: ordered by exchangeSeq; no duplicates
  private readonly msgs: Msg[] = [];

  public [Symbol.iterator](): Iterator<Msg> {
    return this.msgs.values();
  }
  public clear() {
    this.msgs.length = 0;
  }
  public first(): Msg | undefined {
    return this.msgs[0];
  }
  public shift() {
    this.msgs.shift();
  }

  /**
   * Insert the given message in order by exchangeSeq. Replaces existing message
   * for duplicate exchangeSeq.
   */
  public add(msg: Msg) {
    const seq = seqOf(msg);
    const idx = this.msgs.findIndex((msg) => seq <= seqOf(msg));
    if (idx === -1) {
      this.msgs.push(msg);
    } else if (seqOf(this.msgs[idx]) === seq) {
      this.msgs[idx] = msg;
    } else {
      this.msgs.splice(idx, 0, msg);
    }
  }

  /** Remove all messages with exchangeSeq less than given. */
  public deleteSeqsLessThan(seq: number) {
    const idx = this.msgs.findIndex((msg) => seqOf(msg) >= seq);
    if (idx === -1) {
      this.msgs.length = 0;
    } else {
      this.msgs.splice(0, idx + 1);
    }
  }
}

function seqOf(msg: TransportMessage): number {
  return toNumber(msg.header.exchangeSeq ?? 0);
}
