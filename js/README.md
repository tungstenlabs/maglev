```sh
# Create a black-hole in `node_modules` for all the packages.
yarn install

# Build the project with TSUP.
yarn build
```
