#!/bin/sh
set -e

# Bootstrap Typescript generation
./bin/maglev gen ts \
  --any-type \
  --bootstrap \
  --output src/gen/proto_gen_bootstrap.ts \
  --input ../protos/wlabs/maglev/meta.proto

# Main core Typescript generation
./bin/maglev gen ts \
  --any-type \
  --output src/gen/proto_gen.ts \
  --input ../protos/wlabs/maglev/transport.proto \
  --input ../protos/wlabs/maglev/handshake.proto \
  --input ../protos/wlabs/maglev/meta.proto \
  --input proto/**/*.proto

# Test code Typescript generation
./bin/maglev gen ts \
  --runtimeModule ../.. \
  --output src/tests/gen/proto_gen.ts \
  --input src/tests/proto/**/*.proto
