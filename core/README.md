# Building

## Prerequisites

Install Boost 1.66.0

```sh
wget https://boostorg.jfrog.io/artifactory/main/release/1.66.0/source/boost_1_66_0.tar.bz2
tar --bzip2 -xf boost_1_66_0.tar.bz2
sudo apt-get install libbz2-dev
sudo ./b2 install
```

## Build System Notes

> CMake is the worst build system, except for all the others.

**Primer**

The core uses "modern" CMake as the meta build system,
[this blog](https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/)
is a good primer on the subject.

**DocTest**

DocTest is used for testing, it allows tests to be written in the same file as
the source code they test. There are a few build-related consequences to this,
most notably that library targets like `maglev-core-common` also build an
executable that enabled a property `ENABLE_TESTS`.

Because you can't build a `STATIC` lib and then just link that for testing (the
test function symbols won't be exported) we have to build two totally different
targets, one lib one exe. I just use a helper function to configure both targets
instead of repeating code. Which is why all the `CMakeLists.txt` files have a
`function(configure_target target)` definition.

**VSCode**

You'll want to have both the `CMake` and `CMake Tools` extension installed (two
separate extensions). `CMake Tools` provides a new tab that lets you build and
run targets. If, for example, you're trying to write tests, you'll want to
select the correct test target as default, that way VSCode knows `ENABLE_TESTS`
is defined.

**Things I forget**

I always forget what `PUBLIC`, `INTERFACE` and `PRIVATE` mean in CMake.

- `PRIVATE` means the thing to follow is only used to build that target; things
  that consume that target don't need to know anything about it.
- `INTERFACE` means the thing to follow is only used by stuff external to the
  target. Which is to say, the target itself doesn't need it, but it'll be
  needed by code that uses the target.
- `PUBLIC` is just `INTERFACE` + `PRIVATE`
