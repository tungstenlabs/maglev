#include "transport_impl.h"

#include <hello_world.h>

extern "C" {

void* malloc(size_t size) { return (void*)0; }

void free(void* ptr) {}
}

void* realloc(void* ptr, size_t size) { return (void*)0; }

namespace maglev {
namespace core {

int WasmAdd(int a, int b) { return Add(a, b); }

}  // namespace core
}  // namespace maglev
