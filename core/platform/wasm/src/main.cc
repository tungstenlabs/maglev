#include <hello_world.h>

#include <iostream>

int main(int argc, char const* argv[]) {
  std::cout << "Sum is: " << ::maglev::core::Add(1, 2) << std::endl;
  return 0;
}
