#ifndef MAGLEV_CORE_WASM_TRANSPORT_IMPL
#define MAGLEV_CORE_WASM_TRANSPORT_IMPL

#include <cstddef>

extern "C" {

void* malloc(size_t size);
void free(void* ptr);
void* realloc(void* ptr, size_t size);
}

namespace maglev {
namespace core {

int WasmAdd(int a, int b);

}  // namespace core
}  // namespace maglev

#endif  // MAGLEV_CORE_WASM_TRANSPORT_IMPL
