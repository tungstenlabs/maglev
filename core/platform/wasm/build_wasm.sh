#!/bin/sh

# Make sure the Emscripten submodule is pulled
git submodule update --init emscripten

# Use Emscripten to build the entire core.
# This is a great tutorial on using Emscripten with CMake:
# https://thatonegamedev.com/cpp/programming-a-c-game-for-the-web-emscripten/#cmake-settings
# We have emscripten as a submodule here, so we can directly reference the CMake
# file, but we could also have required users activate the emscripten SDK then
# used emcmake instead. For example: emcmake cmake -S .. -B build
cmake -S .. -B build "-DCMAKE_TOOLCHAIN_FILE=emscripten/cmake/Modules/Platform/Emscripten.cmake"
#cmake -DCMAKE_BUILD_TYPE=Release --build build
cmake --build build --config Release

# Copy the .wasm and .js files into the dist directory.
mkdir -p dist
cp build/wasm/maglev-core-wasm.wasm dist
cp build/wasm/maglev-core-wasm.js dist

echo "Done"
