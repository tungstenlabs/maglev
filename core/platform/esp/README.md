## Building

### Checkout the ESP-IDF git submodule

```sh
git submodule update --init --recursive
```

### Install prerequisites

https://docs.espressif.com/projects/esp-idf/en/release-v4.1/get-started/index.html#step-1-install-prerequisites

## Dev Tools

```sh
# Install Dev Tools
./esp-idf/install.sh

# Source shell variables
. ./esp-idf/export.sh
```
