#ifndef MAGLEV_CORE_DESKTOP_TRANSPORT_IMPL_H_
#define MAGLEV_CORE_DESKTOP_TRANSPORT_IMPL_H_

namespace maglev {
namespace core {
namespace desktop {

void SayHello();

}  // namespace desktop
}  // namespace core
}  // namespace maglev

#endif  // MAGLEV_CORE_DESKTOP_TRANSPORT_IMPL_H_
