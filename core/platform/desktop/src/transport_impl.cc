#include "transport_impl.h"

#include <iostream>
#include <string>

namespace maglev {
namespace core {
namespace desktop {

void SayHello() { ::std::cout << "Hello, from Desktop lib!" << ::std::endl; }

}  // namespace desktop
}  // namespace core
}  // namespace maglev

#ifdef ENABLE_TEST

#include <doctest/doctest.h>

#include <websocketpp/client.hpp>
#include <websocketpp/config/asio_no_tls_client.hpp>

typedef websocketpp::client<websocketpp::config::asio_client> client;

TEST_CASE("Testy Mc. Test Face") {
  bool done = false;
  std::string input;

  while (!done) {
    std::cout << "Enter Command: ";
    std::getline(std::cin, input);

    if (input == "quit") {
      done = true;
    } else if (input == "help") {
      std::cout << "\nCommand List:\n"
                << "help: Display this help text\n"
                << "quit: Exit the program\n"
                << std::endl;
    } else {
      std::cout << "Unrecognized Command" << std::endl;
    }
  }

  // STUB
  CHECK(1 == 1);
}

#endif  // ENABLE_TEST
