#include "core_state.h"

#include <cstring>

namespace maglev {
namespace core {

void InitializeCoreState(CoreState* core_state) {
  core_state->routing_table.ws_transport_infos.push_back({
      .node_id = core_state->core_config.relay_node_id,
      .state = TransportState::kUninitialized,
      .url = core_state->core_config.relay_ws_url,
  });

  core_state->initialized = true;
}

void HandleWsTransports(CoreState* core_state) {
  auto& transport_infos = core_state->routing_table.ws_transport_infos;

  for (int i = 0; i < transport_infos.Size(); i++) {
    auto& transport_info = transport_infos[i];

    // TODO: Handle the other transport states.
    switch (transport_info.state) {
      case TransportState::kNacent: {
        if (strcmp()) break;
      }
    }
  }
}

void ProcessCoreState(CoreState* core_state) {
  if (!core_state->initialized) {
    InitializeCoreState(core_state);
  }

  HandleWsTransports(core_state);
}

}  // namespace core
}  // namespace maglev
