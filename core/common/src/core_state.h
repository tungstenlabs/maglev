#ifndef MAGLEV_CORE_COMMON_CORE_STATE_H_
#define MAGLEV_CORE_COMMON_CORE_STATE_H_

#include <stddef.h>

#include <array>
#include <optional>
#include <string>
#include <vector>

#include "collections/slice.h"
#include "utils/block_allocator.h"

namespace maglev {
namespace core {

using ::maglev::core::utils::BlockAllocator;
using ::std::string;
using ::std::vector;

enum class PeerType {
  // Only supports routing, it is not a user-implemented node.
  kCommonRelay,

  // Only supports direct-to traffic, will not relay messages.
  kLeafNode,
};

struct PeerInfo {
  string node_id;
  PeerType type;
};

enum class TransportState {
  // Set be the core to request external code init the transport.
  kUninitialized = 0,

  // Set by external code to indicate a connection is pending.
  kConnectionPending,

  // Set by external code to indicate a healthy connection state.
  kConnected,

  // Indicates that a healthy connection could not be established after some
  // timeout. The core can then move the transport into a disposing state.
  kConnectionFailure,

  // Either the core or external code can set this to indicate that the
  // transport is no longer in use and should be cleaned up (by external code).
  kDisposing,
};

enum class TransportType {
  kWsTransport,
  kRtcTransport,
};

struct WSTransportInfo {
  string node_id;
  TransportState state;
  string url;
};

struct RtcTransportInfo {
  string node_id;
  TransportState state;
};

struct RoutingTable {
  WSTransportInfo common_relay_ws_transport_info;
  vector<RtcTransportInfo> rtc_transport_infos;
};

struct CoreConfig {
  string relay_node_id;
  string relay_ws_url;
  vector<string> ice_servers;
};

// Note: outside code is responsible for zeroing memory before processing.
struct CoreState {
  bool initialized;
  CoreConfig core_config;
  RoutingTable routing_table;
  BlockAllocator block_allocator;
};

void ProcessCoreState(CoreState* core_state);

}  // namespace core
}  // namespace maglev

#endif  // MAGLEV_CORE_COMMON_CORE_STATE_H_
