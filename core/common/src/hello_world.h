#ifndef MAGLEV_CORE_COMMON_HELLO_WORLD
#define MAGLEV_CORE_COMMON_HELLO_WORLD

namespace maglev {
namespace core {

int Add(int a, int b);

}  // namespace core
}  // namespace maglev

#endif  // MAGLEV_CORE_COMMON_HELLO_WORLD
