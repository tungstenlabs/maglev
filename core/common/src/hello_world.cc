#include "hello_world.h"

namespace maglev {
namespace core {

int Add(int a, int b) { return a + b; }

}  // namespace core
}  // namespace maglev

#ifdef ENABLE_TEST

#include <doctest/doctest.h>

TEST_CASE("source test one") { CHECK(1 == 1); }
TEST_CASE("source test two") { CHECK(2 == 2); }

#endif  // ENABLE_TEST
