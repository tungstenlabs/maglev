#ifndef MAGLEV_CORE_COMMON_COLLECTIONS_ARRAYLIKE_H_
#define MAGLEV_CORE_COMMON_COLLECTIONS_ARRAYLIKE_H_

#include <vector>

namespace maglev {
namespace core {
namespace collections {

template <typename T>
class IArraylike {
 public:
  T& operator[](std::size_t n) noexcept = 0;
  std::size_t size() const noexcept = 0;
};

template <typename T>
class VectorArraylike : public ::std::vector<T>, public IArraylike<T> {};

}  // namespace collections
}  // namespace core
}  // namespace maglev

#endif  // MAGLEV_CORE_COMMON_COLLECTIONS_ARRAYLIKE_H_
