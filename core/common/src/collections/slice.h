#ifndef MAGLEV_CORE_COMMON_COLLECTIONS_SLICE_H_
#define MAGLEV_CORE_COMMON_COLLECTIONS_SLICE_H_

namespace maglev {
namespace core {
namespace collections {

template <typename T>
class Slice {
 public:
  inline T& operator[](std::size_t n) noexcept { return data_[n]; }

  inline std::size_t Size() const noexcept { return size_; }

  void ReplaceWith(T* data, size_t size) {
    data_ = data;
    size_ = size;
  }

 public:
  void* user_data;

 private:
  T* data_;
  size_t size_;
};

}  // namespace collections
}  // namespace core
}  // namespace maglev

#endif  // MAGLEV_CORE_COMMON_COLLECTIONS_SLICE_H_
