#ifndef MAGLEV_CORE_COMMON_UTILS_BLOCK_ALLOCATOR_H_
#define MAGLEV_CORE_COMMON_UTILS_BLOCK_ALLOCATOR_H_

#include <array>
#include <cstddef>

const size_t kBlockDataSize = 1500UL;

namespace maglev {
namespace core {
namespace utils {

struct MemoryBlock;

class BlockAllocator {
 public:
  BlockAllocator() = delete;
  BlockAllocator(const void* memory, size_t size);

  unsigned char* MallocBlock();
  void FreeBlock(unsigned char* data_ptr);
  int BlocksRemaining() const;
  int BlocksAllocated() const;
  int BlocksTotal() const;

 private:
  size_t max_blocks_;
  MemoryBlock* first_free_block_;
  unsigned int remaining_blocks_;
};

}  // namespace utils
}  // namespace core
}  // namespace maglev

#endif  // MAGLEV_CORE_COMMON_UTILS_BLOCK_ALLOCATOR_H_
