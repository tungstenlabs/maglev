#ifndef MAGLEV_CORE_COMMON_UTILS_RESULT_H_
#define MAGLEV_CORE_COMMON_UTILS_RESULT_H_

#include <assert.h>

namespace maglev {
namespace core {
namespace utils {

template <typename T, typename TErr = const char*>
class Result {
 public:
  Result(T&& val) : val_(::std::move(val)), has_val_(true);
  Result(TErr&& err) : err_(::std::move(err)), has_val_(false);

  inline static Result<T> Ok(T&& value) {
    return ::std::move(Result(::std::move(value)));
  }

  inline static Result<T> Err(TErr&& error) {
    return ::std::move(Result(::std::move(error)));
  }

  inline bool IsOkay() const { return has_val_; }

  inline bool IsErr() const { return !has_val_; }

  inline T& Val() const {
    assert(has_val_);
    return val_;
  }

  inline T& ValOr(T&& other) const { return has_val_ ? val_ : other; }

  inline Result<T, TErr>& Or(const Result<T, TErr>& other) const {
    return has_val_ ? this : other;
  }

  inline TErr& Err() const {
    assert(!has_val_);
    return err_;
  }

 private:
  Result();

 private:
  T val_;
  TErr err_;
  bool has_val_;
};

}  // namespace utils
}  // namespace core
}  // namespace maglev

#endif  // MAGLEV_CORE_COMMON_UTILS_RESULT_H_
