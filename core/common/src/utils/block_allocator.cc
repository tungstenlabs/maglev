#include "block_allocator.h"

#include <assert.h>

namespace maglev {
namespace core {
namespace utils {

struct MemoryBlock {
  MemoryBlock* next_free_block;
  unsigned char data[kBlockDataSize];
};

BlockAllocator::BlockAllocator(const void* memory, size_t size)
    : first_free_block_((MemoryBlock*)memory),
      max_blocks_(size / sizeof(MemoryBlock)),
      remaining_blocks_(size / sizeof(MemoryBlock)) {
  // Link together the initial list.
  for (int i = 0; i < max_blocks_ - 1; i++) {
    first_free_block_[i].next_free_block = &first_free_block_[i + 1];
  }
}

unsigned char* BlockAllocator::MallocBlock() {
  if (remaining_blocks_ <= 0) {
    return nullptr;
  }

  MemoryBlock* alloc_block = first_free_block_;
  first_free_block_ = alloc_block->next_free_block;
  alloc_block->next_free_block = 0x0;
  remaining_blocks_--;
  return alloc_block->data;
}

void BlockAllocator::FreeBlock(unsigned char* data_ptr) {
  // Manual pointer arithmetic so that outside code doesn't need to know or care
  // about the MemoryBlock structure.
  MemoryBlock* block = (MemoryBlock*)(data_ptr - offsetof(MemoryBlock, data));

  assert(block->next_free_block == 0x0);

  // Prepend it to the linked list.
  block->next_free_block = first_free_block_;
  first_free_block_ = block;
  remaining_blocks_++;
}

int BlockAllocator::BlocksRemaining() const { return remaining_blocks_; }

int BlockAllocator::BlocksAllocated() const {
  return max_blocks_ - remaining_blocks_;
}

int BlockAllocator::BlocksTotal() const { return max_blocks_; }

}  // namespace utils
}  // namespace core
}  // namespace maglev

// #ifdef ENABLE_TESTS

// #include <doctest/doctest.h>

// TEST_CASE("Block allocator alloc/free") {
//   // Allocate a chunk of memory just a little bigger than 4 blocks.
//   auto size = (sizeof(::maglev::core::utils::MemoryBlock) * 4) + 50;
//   auto memory = malloc(size);

//   ::maglev::core::utils::BlockAllocator allocator(memory, size);
//   CHECK(allocator.BlocksTotal() == 4);
//   CHECK(allocator.BlocksAllocated() == 0);
//   CHECK(allocator.BlocksRemaining() == 4);

//   auto chunk_1 = allocator.MallocBlock();

//   auto chunk_2 = allocator.MallocBlock();
//   *(int*)chunk_2 = 2;

//   auto chunk_3 = allocator.MallocBlock();
//   auto chunk_4 = allocator.MallocBlock();

//   CHECK(allocator.BlocksAllocated() == 4);
//   CHECK(allocator.BlocksRemaining() == 0);

//   allocator.FreeBlock(chunk_2);
//   CHECK(allocator.BlocksAllocated() == 3);
//   CHECK(allocator.BlocksRemaining() == 1);

//   // Ensure it gives back the correct block after Chunk 2 is freed then
//   alloced
//   // again.
//   chunk_2 = allocator.MallocBlock();
//   CHECK((*(int*)chunk_2) == 2);

//   // Should return nullptr if you call malloc too many times.
//   CHECK(allocator.MallocBlock() == nullptr);
// }

// #endif  // TESTING_ENABLED
