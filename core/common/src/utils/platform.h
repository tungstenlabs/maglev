#ifndef MAGLEV_CORE_COMMON_UTILS_PLATFORM_H_
#define MAGLEV_CORE_COMMON_UTILS_PLATFORM_H_

namespace maglev {
namespace core {
namespace utils {

extern int printf(const char* format, ...);

}  // namespace utils
}  // namespace core
}  // namespace maglev

#endif  // MAGLEV_CORE_COMMON_UTILS_PLATFORM_H_
