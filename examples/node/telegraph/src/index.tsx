import { channel, ChannelSender, Maglev, NodeRef } from '@wlabs/maglev';
import { render } from 'ink';
import React from 'react';
import yargs from 'yargs';
import { App } from './app';
import { wlabs } from './proto_gen';

const MAX_MESSAGES = 100;

const maglev = new Maglev({
  relay: { url: "ws://localhost:3035" },
  wrtc: { iceServers: [{ urls: "stun:stun3.l.google.com" }] },
});

async function serveChat() {
  // We are hosting a chat server. We will store an in-memory buffer of messages
  // and when we receive a message we will add it to this buffer, and send it
  // out to all connected clients.
  let nextId = 1;
  const messages: wlabs.examples.Msg[] = [];
  const outboundSenders: ChannelSender<wlabs.examples.Msg>[] = [];

  // A helper to append a message and broadcast that message to all clients.
  const broadcastMessage = (msg: wlabs.examples.Msg_WithDefaultValues) => {
    msg.id = nextId++;
    messages.push(msg);
    messages.splice(0, messages.length - MAX_MESSAGES);
    for (const sender of outboundSenders) {
      sender.send(msg);
    }
  };

  wlabs.examples.Telegraph.registerHandler(maglev, {
    // This is a Maglev RPC Handler, it will be called when someone else invokes
    // the `chat` RPC on us. The streamed version we use here takes in a
    // ChannelReceiver<Msg> for inbound messages, and returns a
    // ChannelReceiver<Msg> for outbound messages. There are other valid return
    // types though, like an async iterator.
    chat: (req, _ctx) => {
      console.warn('Handling:', _ctx.peerAuthInfo.nodeId);
      // Make a channel that we can use later to send this client messages from
      // other peers.
      const [sender, receiver] = channel<wlabs.examples.Msg>();
      outboundSenders.push(sender);

      // Send the last 20 messages on the peer right now.
      for (const msg of messages.slice(-20)) {
        sender.send(msg);
      }

      // When we get a message from any peer, add it to the message list and
      // forward it on to all other peers.
      void (async () => {
        for await (const e of req) {
          if (e.err) {
            throw e.err;
          }

          broadcastMessage(e.msg);
        }
      })();

      return receiver;
    },
  });

  // HACK
  void (await maglev.reachable.get());
}

async function joinChat(name: string, hostId?: string) {
  // This is the ID of our own node.
  const ourId = (await maglev.nodes.self()).nodeId;

  // Default to connecting to ourself.
  hostId ??= ourId;

  // We need to call a streaming RPC on the "server" node. Nothing as far as
  // Maglev is concerned is special about this node, it just happens to be
  // hosting the RPC we need to call. We start by getting a NodeRef, then
  // synthesizing a client from that node ref and our service.
  const serverNodeRef = maglev.nodes.get(hostId);
  const client = wlabs.examples.Telegraph.createClient(serverNodeRef);

  // Then create a channel to send data to the remote node, and invoke the RPC
  // which will return a channel to receive data from.
  const [sender, remoteReceiver] = channel<wlabs.examples.Msg>();
  const receiver = client.chat(remoteReceiver);

  // Finally we can render the chat app. Everything else is just for UI, all
  // Maglev/RPC related stuff is in this file.
  render(
    <App
      name={name}
      hostId={hostId}
      ourId={ourId}
      messageSender={sender}
      messageReceiver={receiver}
    />
  );
}

export function main(argv: string[]) {
  yargs(argv.slice(2))
    .usage('Usage: $0 <command> [options]')
    .command(
      'host <name>',
      'Serve and join a chat server',
      () => {},
      async (args) => {
        // When we are the host, we create a 'server' handler, then just join
        // ourselves over the loopback transport.
        serveChat();
        joinChat(args.name as string, 'loopback');
      }
    )
    .command(
      'join <hostId> <name>',
      'Join an existing chat server',
      () => {},
      async (args) => {
        // When we are joining an existing host, we just join by their Node ID.
        joinChat(args.name as string, args.hostId as string);
      }
    )
    .demandCommand()
    .help('h')
    .alias('h', 'help').argv;
}

main(process.argv);
