import React from 'react';
import { Box } from 'ink';

export const Fullscreen: React.FC = (props) => {
  const [size, setSize] = React.useState({
    columns: process.stdout.columns,
    rows: process.stdout.rows,
  });

  React.useEffect(() => {
    const onResize = () => {
      setSize({
        columns: process.stdout.columns,
        rows: process.stdout.rows - 1,
      });
    };

    process.stdout.on('resize', onResize);
    process.stdout.write('\x1b[?1049h');
    return () => {
      process.stdout.off('resize', onResize);
      process.stdout.write('\x1b[?1049l');
    };
  }, []);

  return (
    <Box width={size.columns} height={size.rows}>
      {props.children}
    </Box>
  );
};

process.on('exit', () => process.stdout.write('\x1b[?1049l'));
