import { ChannelReceiver, ChannelSender } from '@wlabs/maglev';
import { Box, Newline, Text } from 'ink';
import TextInput from 'ink-text-input';
import React from 'react';
import { Fullscreen } from './fullscreen';
import { wlabs } from './proto_gen';

export interface AppProps {
  name: string;
  hostId: string;
  ourId: string;
  messageSender: ChannelSender<wlabs.examples.Msg>;
  messageReceiver: ChannelReceiver<wlabs.examples.Msg>;
}

export const App: React.FunctionComponent<AppProps> = (props) => {
  const [messages, setMessages] = React.useState<wlabs.examples.Msg[]>([]);
  const [typingMsg, setTypingMsg] = React.useState<string>('');

  React.useEffect(() => {
    void (async () => {
      for await (const e of props.messageReceiver) {
        e.msg && setMessages((existing) => [...existing, e.msg]);
      }
    })();
  }, []);

  return (
    <Fullscreen>
      <Box width="100%" height="100%" flexDirection="column">
        <Box
          height={4}
          paddingLeft={1}
          paddingRight={1}
          borderColor="green"
          borderStyle="round"
        >
          <Text>
            <Text>Chat ID: {props.hostId}</Text>
            <Newline />
            <Text>Our ID: {props.ourId}</Text>
          </Text>
        </Box>
        <Box flexGrow={1} flexDirection="column">
          {messages.map((msg) => (
            <Box
              key={msg.id}
              borderStyle="round"
              borderColor={getNameColor(msg.sender || 'Unknown')}
              marginLeft={1}
              marginRight={1}
              paddingLeft={1}
              paddingRight={1}
            >
              <Text>
                <Text bold color={getNameColor(msg.sender || 'Unknown')}>
                  {msg.sender || 'Unknown'}:{' '}
                </Text>
                <Text>{msg.text}</Text>
              </Text>
            </Box>
          ))}
        </Box>
        <Box height={1}>
          <Box marginRight={1}>
            <Text>{'>'}</Text>
          </Box>
          <TextInput
            value={typingMsg}
            onChange={setTypingMsg}
            onSubmit={(text) => {
              props.messageSender.send({ sender: props.name, text });
              setTypingMsg('');
            }}
          />
        </Box>
      </Box>
    </Fullscreen>
  );
};

function getNameColor(name: string): string {
  return chatColors[name.charCodeAt(0) % chatColors.length];
}

const chatColors = [
  'red',
  'green',
  'yellow',
  'blue',
  'magenta',
  'cyan',
  'redBright',
  'greenBright',
  'yellowBright',
  'blueBright',
  'magentaBright',
  'cyanBright',
];
