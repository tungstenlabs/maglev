import { Maglev, NodeRef } from "@wlabs/maglev";
import { hello_world } from "./proto_gen";

async function main(args: string[]) {
  const maglev = new Maglev({
    relay: {
      url: "ws://localhost:3035",
    },
    wrtc: {
      iceServers: [
        {
          urls: "stun:stun3.l.google.com",
        },
      ],
    },
  });

  const nodeId = (await maglev.nodes.self()).nodeId;
  console.info("Initialized node:", nodeId);

  console.info("Connecting to relay...");
  if (!(await maglev.reachable.whenEqual(true, 5000))) {
    console.error("Connection timed out!");
    return;
  }

  if (args[0]) {
    await runClient(maglev.nodes.get(args[0]));
  } else {
    await runServer(maglev);
  }
}

async function runClient(serverNodeRef: NodeRef) {
  console.info("Connecting to server...");
  if (!(await serverNodeRef.connect(5000))) {
    console.error("Connection timed out!");
    return;
  }
  const client = hello_world.HelloService.createClient(serverNodeRef);
  console.info("Sending Hello...");
  const res = await client.hello({ body: "Hello server!" });
  console.info("Got response:", res.body);
}

async function runServer(maglev: Maglev) {
  hello_world.HelloService.registerHandler(maglev, {
    hello: (req) => {
      console.info("Got request:", req.body);
      return { body: "Hello client!" };
    },
  });
  console.info("Server running!");
  await new Promise(() => {}); // Wait forever
}

main(process.argv.slice(2)).then(() => process.exit());
