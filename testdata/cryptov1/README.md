# Maglev crypto V1 test data

## Browser test data:

```js
signingPair = await crypto.subtle.generateKey(
  { name: "ECDSA", namedCurve: "P-256" },
  true,
  ["sign"]
);
signingKeyJWK = await crypto.subtle.exportKey("jwk", signingPair.privateKey);

payload = "\x00\x0atestPrefix\x00\x08sigPart1\x00\x0asigPartTwo";
signature = await crypto.subtle.sign(
  { name: "ECDSA", hash: "SHA-256" },
  signingPair.privateKey,
  new TextEncoder().encode(payload)
);
signingSignature = btoa(
  String.fromCharCode.apply(null, new Uint8Array(signature))
);

keyderivationKey = await crypto.subtle.importKey(
  "raw",
  new TextEncoder().encode("testSecret"),
  "HKDF",
  false,
  ["deriveBits"]
);
output = await crypto.subtle.deriveBits(
  {
    name: "HKDF",
    hash: "SHA-256",
    info: new TextEncoder().encode("testInfo"),
    salt: new Uint8Array(),
  },
  keyderivationKey,
  32
);
keyderivationOutput = btoa(
  String.fromCharCode.apply(null, new Uint8Array(output))
);

keyagreementPair = await crypto.subtle.generateKey(
  { name: "ECDH", namedCurve: "P-256" },
  true,
  ["deriveBits"]
);
keyagreementKeyJWK = await crypto.subtle.exportKey(
  "jwk",
  keyagreementPair.privateKey
);
output = await crypto.subtle.deriveBits(
  { name: "ECDH", public: keyagreementPair.publicKey },
  keyagreementPair.privateKey,
  128
);
keyagreementOutput = btoa(
  String.fromCharCode.apply(null, new Uint8Array(output))
);

console.log(
  JSON.stringify({
    signingKeyJWK,
    signingSignature,
    keyderivationOutput,
    keyagreementKeyJWK,
    keyagreementOutput,
  })
);
```
